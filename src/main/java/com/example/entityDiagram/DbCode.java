package com.example.entityDiagram;

import com.example.entityDiagram.Repo.*;
import com.example.entityDiagram.entity.*;
import com.example.entityDiagram.entity.businessunit.BusinessUnit;
import com.example.entityDiagram.entity.businessunit.EmpBusinessUnit;
import com.example.entityDiagram.entity.certificationtask.CertTask;
import com.example.entityDiagram.entity.certificationtask.EmpCertTask;
import com.example.entityDiagram.entity.certificationtaskdetails.*;
import com.example.entityDiagram.entity.employeedetails.Company;
import com.example.entityDiagram.entity.employeedetails.Function;
import com.example.entityDiagram.entity.employeedetails.Geography;
import com.example.entityDiagram.entity.processarea.Process;
import com.example.entityDiagram.entity.processarea.ProcessArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DbCode  implements ApplicationRunner {

    @Autowired
    private AdditionalDocumentRepo additionalDocumentRepo;
    @Autowired
    private AssetDetailsRepo assetDetailsRepo;
    @Autowired
    private EmployeeBusinessUnitRepo employeeBusinessUnitRepo;
    @Autowired
    private BusinessUnitRepo businessUnitRepo;
    @Autowired
    private CertificateRepo certificateRepo;
    @Autowired
    private CertificationTaskRepo certificationTaskRepo;
    @Autowired
    private EmployeeCertificationTaskDetailsRepo employeeCertificationTaskDetailsRepo;
    @Autowired
    private EmployeeCertificationTaskRepo employeeCertificationTaskRepo;
    @Autowired
    private EmployeeProcessAreaRepo employeeProcessAreaRepo;
    @Autowired
    private EmployeeProcessRepo employeeProcessRepo;
    @Autowired
    private EmployeeRepo employeeRepo;
    @Autowired
    private ProcessAreaRepo processAreaRepo;
    @Autowired
    private ProcessRepo processRepo;
    @Autowired
    private RemarksRepo remarksRepo;
    @Autowired
    private FunctionRepo functionRepo;
    @Autowired
    private GeographyRepo geographyRepo;
    @Autowired
    private CompanyRepo companyRepo;

    @Override
    public void run(ApplicationArguments args) {

        Employee shrikant = new Employee("1100110","Shrikant","123");

        Employee X = new Employee("1100111","Anand","123");
        Employee U = new Employee("1100112","Rama","123");
        Employee Y = new Employee("1100113","Krishna","123");

        employeeRepo.saveAll(Arrays.asList(shrikant,X,U,Y));

        BusinessUnit TCS_GLOBAL = new BusinessUnit("TCS-GLOBAL","TCS");
        BusinessUnit CL_BFSI = new BusinessUnit("CL-BFSI","Cluster");
        BusinessUnit IS_BFSI_UK_2_Parent = new BusinessUnit("IS_BFSI-UK-2-Parent","Capital Place");
        BusinessUnit IS_BFSI_UK_23_Group3 = new BusinessUnit("IS-BFSI-UK-2.3-Group3","Industry Solution");

        businessUnitRepo.saveAll(Arrays.asList(TCS_GLOBAL,CL_BFSI,IS_BFSI_UK_2_Parent,IS_BFSI_UK_23_Group3));

        //--------------------Emp Business Unit Ready
        EmpBusinessUnit COO_Emp_BusinessUnit = new EmpBusinessUnit(TCS_GLOBAL,"COO",new ArrayList<>());
        EmpBusinessUnit X_Emp_BusinessUnit = new EmpBusinessUnit(CL_BFSI,"Product Owning Head",new ArrayList<>());
        EmpBusinessUnit U_Emp_BusinessUnit = new EmpBusinessUnit(IS_BFSI_UK_2_Parent, "Product Head",new ArrayList<>());
        EmpBusinessUnit Y_Emp_BusinessUnit = new EmpBusinessUnit(IS_BFSI_UK_23_Group3,"Ip Asset Owner",new ArrayList<>());

        COO_Emp_BusinessUnit.setEmpId(shrikant.getEmpId());

        X_Emp_BusinessUnit.setBusinessUnit(CL_BFSI);
        X_Emp_BusinessUnit.setParentBusinessUnit(COO_Emp_BusinessUnit);
        X_Emp_BusinessUnit.setReportingToEmpId(shrikant.getEmpId());
        X_Emp_BusinessUnit.setEmpId(X.getEmpId());


        U_Emp_BusinessUnit.setParentBusinessUnit(X_Emp_BusinessUnit);
        U_Emp_BusinessUnit.setReportingToEmpId(X.getEmpId());
        U_Emp_BusinessUnit.setEmpId(U.getEmpId());

        Y_Emp_BusinessUnit.setParentBusinessUnit(U_Emp_BusinessUnit);
        Y_Emp_BusinessUnit.setReportingToEmpId(U.getEmpId());
        Y_Emp_BusinessUnit.setEmpId(Y.getEmpId());



        employeeBusinessUnitRepo.saveAll(Arrays.asList(X_Emp_BusinessUnit,U_Emp_BusinessUnit,Y_Emp_BusinessUnit,COO_Emp_BusinessUnit));


        //-------------------------------------------

        //-----------------------certTask
        CertTask IPAndETask = new CertTask("IP & Engineering Certificate",
                LocalDate.of(2021,1,10),
                LocalDate.of(2021,4,10),
                LocalDate.of(2021,4,12)
        );


        CertTask contractualCompliance = new CertTask("Contractual Compliance Certificate",
                LocalDate.of(2021,1,10),
                LocalDate.of(2021,4,10),
                LocalDate.of(2021,4,12)
        );

        certificationTaskRepo.saveAll(Arrays.asList(IPAndETask,contractualCompliance));
        //---------------------------------


        //----------------------------------Process
        Process IPAndEProcessArea1Process1 = new Process(" Necessary Filling completed for Trademark, Patent, Copyright and/or Design as per applicable");
        Process IPAndEProcessArea1Process2 = new Process(" IPR filling done in relevant jurisdiction where TCS IP asset is to be positioned");
        Process IPAndEProcessArea1Process3 = new Process(" Any deviation notified in the assessment report for this TCS IP asset has been actioned to meet the IP safe requirement");
        Process IPAndEProcessArea1Process4 = new Process(" Asset has been verified for avoidance of any IP containment and infringement - where applicable(or recommended by IP&E team or TCS legal team) - through MCD and FTO");
        Process IPAndEProcessArea1Process5 = new Process(" All legal issues are resolved for this TCS IP asset");


        final List<Process> processesForProcessArea1IpAndE = Arrays.asList(IPAndEProcessArea1Process1, IPAndEProcessArea1Process2, IPAndEProcessArea1Process3, IPAndEProcessArea1Process4, IPAndEProcessArea1Process5);

        processRepo.saveAll(processesForProcessArea1IpAndE);

        Process ContractProcessArea1Process1 = new Process(" All associate deployment/allocation/movement restrictions from client, partner contracts are captured from the client/partner contracts");
        Process ContractProcessArea1Process2 = new Process(" All associate deployment/allocation/movement restrictions from client, partner contracts are are enforced as per the client/partner contracts");

        Process ContractProcessArea2Process1 = new Process(" DLP measures are implemented across all the desktop/laptops/IT/ systems(including client owned). Where are not implemented, awareness and random checks carried out to ensure compliance");
        Process ContractProcessArea2Process2 = new Process(" Business Associates neither have production access nor privileged accesses");
        Process ContractProcessArea2Process3 = new Process(" Data breach liabilities are limited or not applicable if breach is caused by a) inadequate or lack of security controls by clients, b)staff augmentation positions");

        final List<Process> processesForProcessArea1Contract = Arrays.asList(ContractProcessArea1Process1, ContractProcessArea1Process2);
        final List<Process> processesForProcessArea2Contract = Arrays.asList(ContractProcessArea2Process1, ContractProcessArea2Process2, ContractProcessArea2Process3);

        processRepo.saveAll(processesForProcessArea1Contract);
        processRepo.saveAll(processesForProcessArea2Contract);
        //-------------------------------------------

        //---------------------------------------ProcessArea
        ProcessArea IPAndEProcessArea1 = new ProcessArea("Intellectual Property and Engineering Processes");

        IPAndEProcessArea1.setProcesses(processesForProcessArea1IpAndE);

        final List<ProcessArea> processAreasForIPAndE = Arrays.asList(IPAndEProcessArea1/*, IPAndEProcessArea2*/);
        processAreaRepo.saveAll(processAreasForIPAndE);


        ProcessArea ContractProcessArea1 = new ProcessArea("Client Contracts - Cool Off Requirements");
        ProcessArea ContractProcessArea2 = new ProcessArea("Client Contracts - High or Unlimited Usability");

        ContractProcessArea1.setProcesses(processesForProcessArea1Contract);
        ContractProcessArea2.setProcesses(processesForProcessArea2Contract);

        final List<ProcessArea> processAreasForContract = Arrays.asList(ContractProcessArea1,ContractProcessArea2);
        processAreaRepo.saveAll(processAreasForContract);

        //------------------------------------------

        //----------------Employee process
        EmpProcess X_IP_EmpProcess1 = new EmpProcess(IPAndEProcessArea1Process1,"No","High","");
        EmpProcess X_IP_EmpProcess2 = new EmpProcess(IPAndEProcessArea1Process2,"No","High","");
        EmpProcess X_IP_EmpProcess3 = new EmpProcess(IPAndEProcessArea1Process3,"No","High","");
        EmpProcess X_IP_EmpProcess4 = new EmpProcess(IPAndEProcessArea1Process4,"No","High","");
        EmpProcess X_IP_EmpProcess5 = new EmpProcess(IPAndEProcessArea1Process5,"No","High","");

        final List<EmpProcess> X_IPAndE_EmpProcessList1 = Arrays.asList(X_IP_EmpProcess1, X_IP_EmpProcess2, X_IP_EmpProcess3,X_IP_EmpProcess4,X_IP_EmpProcess5);

        EmpProcess Y_IP_EmpProcess1 = new EmpProcess(IPAndEProcessArea1Process1,"No","High","");
        EmpProcess Y_IP_EmpProcess2 = new EmpProcess(IPAndEProcessArea1Process2,"No","High","");
        EmpProcess Y_IP_EmpProcess3 = new EmpProcess(IPAndEProcessArea1Process3,"No","High","");
        EmpProcess Y_IP_EmpProcess4 = new EmpProcess(IPAndEProcessArea1Process4,"No","High","");
        EmpProcess Y_IP_EmpProcess5 = new EmpProcess(IPAndEProcessArea1Process5,"No","High","");

        final List<EmpProcess> Y_IPAndE_EmpProcessList1 = Arrays.asList(Y_IP_EmpProcess1, Y_IP_EmpProcess2, Y_IP_EmpProcess3, Y_IP_EmpProcess4, Y_IP_EmpProcess5);

        EmpProcess U_IP_EmpProcess1 = new EmpProcess(IPAndEProcessArea1Process1,"No","High","");
        EmpProcess U_IP_EmpProcess2 = new EmpProcess(IPAndEProcessArea1Process2,"No","High","");
        EmpProcess U_IP_EmpProcess3 = new EmpProcess(IPAndEProcessArea1Process3,"No","High","");
        EmpProcess U_IP_EmpProcess4 = new EmpProcess(IPAndEProcessArea1Process4,"No","High","");
        EmpProcess U_IP_EmpProcess5 = new EmpProcess(IPAndEProcessArea1Process5,"No","High","");

        final List<EmpProcess> U_IPAndE_EmpProcessList1 = Arrays.asList(U_IP_EmpProcess1, U_IP_EmpProcess2, U_IP_EmpProcess3, U_IP_EmpProcess4, U_IP_EmpProcess5);

        //-------------------------------


        //---------------------------EmpProcessArea
        EmpProcessArea X_IPAndE_Emp_Process_Area1 = new EmpProcessArea(IPAndEProcessArea1,X_IPAndE_EmpProcessList1);


        final List<EmpProcessArea> X_IPAndE_Emp_Process_Areas = Arrays.asList(X_IPAndE_Emp_Process_Area1);

        employeeProcessAreaRepo.saveAll(X_IPAndE_Emp_Process_Areas);
        employeeProcessAreaRepo.flush();

        EmpProcessArea Y_IPAndE_Emp_Process_Area1 = new EmpProcessArea(IPAndEProcessArea1,Y_IPAndE_EmpProcessList1);

        final List<EmpProcessArea> Y_IPAndE_Emp_Process_Areas = Arrays.asList(Y_IPAndE_Emp_Process_Area1);

        employeeProcessAreaRepo.saveAll(Y_IPAndE_Emp_Process_Areas);
        employeeProcessAreaRepo.flush();


        EmpProcessArea U_IPAndE_Emp_Process_Area1 = new EmpProcessArea(IPAndEProcessArea1,U_IPAndE_EmpProcessList1);

        final List<EmpProcessArea> U_IPAndE_Emp_Process_Areas = Arrays.asList(U_IPAndE_Emp_Process_Area1/*, U_IPAndE_Emp_Process_Area2*/);


        employeeProcessAreaRepo.saveAll(U_IPAndE_Emp_Process_Areas);
        employeeProcessAreaRepo.flush();
        //-----------------------------------------




        //-----------------Remarks

        AssetDetails Y_IPAndE_AssetDetail = new AssetDetails(Y.getEmpId(),"ignio iSight","Laptop","Active","V1.0");
        assetDetailsRepo.save(Y_IPAndE_AssetDetail);

        Remark Y_IPAndE_Remark1 = new Remark(Y_IPAndE_AssetDetail.getAssetName(),"No","");
        remarksRepo.save(Y_IPAndE_Remark1);

        AssetDetails U_IPAndE_AssetDetail = new AssetDetails(U.getEmpId(),"TCS IP Asset Management System (TIPAMS)","Laptop","Active","V1.0");
        assetDetailsRepo.save(U_IPAndE_AssetDetail);

        Remark U_IPAndE_Remark1 = new Remark(U_IPAndE_AssetDetail.getAssetName(),"No","");
        remarksRepo.save(U_IPAndE_Remark1);
        //


        //------------------------ Additional Document
        AdditionalDocument X_IPAndE_Additional_document1 = new AdditionalDocument("X's_FILE_1","#","descritpion");
        additionalDocumentRepo.save(X_IPAndE_Additional_document1);

        AdditionalDocument Y_IPAndE_Additional_document1 = new AdditionalDocument("Y's_FILE_1","#","descritpion");
        additionalDocumentRepo.save(Y_IPAndE_Additional_document1);

        AdditionalDocument U_IPAndE_Additional_document1 = new AdditionalDocument("U's_FILE_1","#","descritpion");
        additionalDocumentRepo.save(U_IPAndE_Additional_document1);
        //


        //Emp certification Task Details
        EmpCertificateTaskDetails X_IPAndE_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        X_IPAndE_SavedDETAILS.setEmpProcessAreas(X_IPAndE_Emp_Process_Areas);
        X_IPAndE_SavedDETAILS.setRemarks(null/*Arrays.asList(X_IPAndE_Remark1)*/);
        X_IPAndE_SavedDETAILS.setAdditionalDocuments(Arrays.asList(X_IPAndE_Additional_document1));
//        X_IPAndE_SavedDETAILS.setCertificate(X_IPAndE_Certificate);

        EmpCertificateTaskDetails X_IPAndE_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        X_IPAndE_SubmittedDetails.setEmpProcessAreas(null);
        X_IPAndE_SubmittedDetails.setRemarks(null);
        X_IPAndE_SubmittedDetails.setAdditionalDocuments(null);
        X_IPAndE_SubmittedDetails.setCertificate(null);
        X_IPAndE_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(X_IPAndE_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(X_IPAndE_SubmittedDetails);

        employeeCertificationTaskDetailsRepo.flush();

        EmpCertificateTaskDetails Y_IPAndE_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
//        Y_IPAndE_SavedDETAILS.setEmpCertTaskId();
        Y_IPAndE_SavedDETAILS.setEmpProcessAreas(Y_IPAndE_Emp_Process_Areas);
        Y_IPAndE_SavedDETAILS.setRemarks(Arrays.asList(Y_IPAndE_Remark1));
        Y_IPAndE_SavedDETAILS.setAdditionalDocuments(Arrays.asList(Y_IPAndE_Additional_document1));
//        Y_IPAndE_SavedDETAILS.setCertificate(Y_IPAndE_Certificate);

        EmpCertificateTaskDetails Y_IPAndE_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
//        Y_IPAndE_SavedDETAILS.setEmpCertTaskId();
        Y_IPAndE_SubmittedDetails.setEmpProcessAreas(null);
        Y_IPAndE_SubmittedDetails.setRemarks(null);
        Y_IPAndE_SubmittedDetails.setAdditionalDocuments(null);
        Y_IPAndE_SubmittedDetails.setCertificate(null);

        employeeCertificationTaskDetailsRepo.save(Y_IPAndE_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(Y_IPAndE_SubmittedDetails);
        employeeCertificationTaskDetailsRepo.flush();


        EmpCertificateTaskDetails U_IPAndE_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
//        X_IPAndE_SavedDETAILS.setEmpCertTaskId();
        U_IPAndE_SavedDETAILS.setEmpProcessAreas(U_IPAndE_Emp_Process_Areas);
        U_IPAndE_SavedDETAILS.setRemarks(Arrays.asList(U_IPAndE_Remark1));
        U_IPAndE_SavedDETAILS.setAdditionalDocuments(Arrays.asList(U_IPAndE_Additional_document1));
//        U_IPAndE_SavedDETAILS.setCertificate(U_IPAndE_Certificate);

        EmpCertificateTaskDetails U_IPAndE_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
//        X_IPAndE_SavedDETAILS.setEmpCertTaskId();
        U_IPAndE_SubmittedDetails.setEmpProcessAreas(null);
        U_IPAndE_SubmittedDetails.setRemarks(null);
        U_IPAndE_SubmittedDetails.setAdditionalDocuments(null);
        U_IPAndE_SubmittedDetails.setCertificate(null);

        employeeCertificationTaskDetailsRepo.save(U_IPAndE_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(U_IPAndE_SubmittedDetails);
        employeeCertificationTaskDetailsRepo.flush();


        X_IPAndE_Additional_document1.setEmpCertificationTaskDetailsId(X_IPAndE_SavedDETAILS.getId());
        Y_IPAndE_Additional_document1.setEmpCertificationTaskDetailsId(Y_IPAndE_SavedDETAILS.getId());
        U_IPAndE_Additional_document1.setEmpCertificationTaskDetailsId(U_IPAndE_SavedDETAILS.getId());

        additionalDocumentRepo.saveAll(Arrays.asList(X_IPAndE_Additional_document1,Y_IPAndE_Additional_document1,U_IPAndE_Additional_document1));

        //---------------EmpBusinessUnit

        //------------------------

        EmpCertTask X_IPAndE_Task = new EmpCertTask(LocalDate.of(2021,6,1),X.getEmpId(),"Pending", X_IPAndE_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        X_IPAndE_Task.setCertTask(IPAndETask);
        X_IPAndE_Task.setEmpBusinessUnit(X_Emp_BusinessUnit);
        X_IPAndE_Task.setCertifierName(X.getName());
        X_IPAndE_Task.setEmployeeId(X.getEmpId());
        X_IPAndE_Task.setSavedDetails(X_IPAndE_SavedDETAILS);
        X_IPAndE_Task.setSubmittedDetails(X_IPAndE_SubmittedDetails);
        employeeCertificationTaskRepo.save(X_IPAndE_Task);


        setCertificationTaskIdInEmpProcessArea(X_IPAndE_Emp_Process_Areas,X_IPAndE_Task.getId());

        employeeProcessAreaRepo.saveAll(X_IPAndE_Emp_Process_Areas);

        EmpCertTask Y_IPAndE_Task = new EmpCertTask(
                LocalDate.of(2021,6,1),Y.getEmpId(),"Pending", Y_IPAndE_SavedDETAILS,null/*Y_IPAndE_SubmittedDetails*/);

        Y_IPAndE_Task.setCertTask(IPAndETask);
        Y_IPAndE_Task.setEmpBusinessUnit(Y_Emp_BusinessUnit);
        Y_IPAndE_Task.setCertifierName(Y.getName());
        Y_IPAndE_Task.setEmployeeId(Y.getEmpId());
        Y_IPAndE_Task.setSavedDetails(Y_IPAndE_SavedDETAILS);
        Y_IPAndE_Task.setSubmittedDetails(Y_IPAndE_SubmittedDetails);
        employeeCertificationTaskRepo.save(Y_IPAndE_Task);

        setCertificationTaskIdInEmpProcessArea(Y_IPAndE_Emp_Process_Areas,Y_IPAndE_Task.getId());
        employeeProcessAreaRepo.saveAll(Y_IPAndE_Emp_Process_Areas);

        EmpCertTask U_IPAndE_Task = new EmpCertTask(LocalDate.of(2021,6,1),U.getEmpId(),"Pending", U_IPAndE_SavedDETAILS ,null/*U_IPAndE_SubmittedDetails*/);
        U_IPAndE_Task.setCertTask(IPAndETask);
        U_IPAndE_Task.setEmpBusinessUnit(U_Emp_BusinessUnit);
        U_IPAndE_Task.setCertifierName(U.getName());
        U_IPAndE_Task.setEmployeeId(U.getEmpId());
        U_IPAndE_Task.setSavedDetails(U_IPAndE_SavedDETAILS);
        U_IPAndE_Task.setSubmittedDetails(U_IPAndE_SubmittedDetails);
        employeeCertificationTaskRepo.save(U_IPAndE_Task);

        //-----employee certification task details associated with Emp Cert Task id
        X_IPAndE_SavedDETAILS.setEmpCertTaskId(X_IPAndE_Task.getId());
        Y_IPAndE_SavedDETAILS.setEmpCertTaskId(Y_IPAndE_Task.getId());
        U_IPAndE_SavedDETAILS.setEmpCertTaskId(U_IPAndE_Task.getId());

        X_IPAndE_SavedDETAILS.setEmpCertTaskId(X_IPAndE_Task.getId());
        Y_IPAndE_SavedDETAILS.setEmpCertTaskId(Y_IPAndE_Task.getId());
        U_IPAndE_SavedDETAILS.setEmpCertTaskId(Y_IPAndE_Task.getId());

        employeeCertificationTaskDetailsRepo.saveAll(
                Arrays.asList(X_IPAndE_SavedDETAILS,Y_IPAndE_SavedDETAILS,U_IPAndE_SavedDETAILS,
                        X_IPAndE_SubmittedDetails,Y_IPAndE_SubmittedDetails,U_IPAndE_SubmittedDetails));


        //--------------------

        setCertificationTaskIdInEmpProcessArea(U_IPAndE_Emp_Process_Areas,U_IPAndE_Task.getId());
        employeeCertificationTaskDetailsRepo.saveAll(Arrays.asList(X_IPAndE_SavedDETAILS,Y_IPAndE_SavedDETAILS,U_IPAndE_SavedDETAILS));

        employeeProcessAreaRepo.saveAll(U_IPAndE_Emp_Process_Areas);
        employeeProcessAreaRepo.saveAll(Y_IPAndE_Emp_Process_Areas);
        employeeProcessAreaRepo.saveAll(X_IPAndE_Emp_Process_Areas);

        X_Emp_BusinessUnit.setEmpCertTasks(Arrays.asList(X_IPAndE_Task));
        Y_Emp_BusinessUnit.setEmpCertTasks(Arrays.asList(Y_IPAndE_Task));
        U_Emp_BusinessUnit.setEmpCertTasks(Arrays.asList(U_IPAndE_Task));
//        employeeBusinessUnitRepo.saveAll(Arrays.asList(X_Emp_BusinessUnit,Y_Emp_BusinessUnit,U_Emp_BusinessUnit));

        //-----------------remark association with employee cert task id
//        X_IPAndE_Remark1.setEmpCertificationTaskId(X_IPAndE_Task.getId());
        Y_IPAndE_Remark1.setEmpCertificationTaskId(Y_IPAndE_Task.getId());
        U_IPAndE_Remark1.setEmpCertificationTaskId(U_IPAndE_Task.getId());

        remarksRepo.saveAll(Arrays.asList(/*X_IPAndE_Remark1,*/Y_IPAndE_Remark1,U_IPAndE_Remark1));
        //------------------------


        //------------------process association with


        employeeRepo.saveAll(Arrays.asList(shrikant,X,U,Y));
        employeeBusinessUnitRepo.saveAll(Arrays.asList(X_Emp_BusinessUnit,U_Emp_BusinessUnit,Y_Emp_BusinessUnit,COO_Emp_BusinessUnit));


        Function COO_Function = new Function("COO");
        Function HR_Function = new Function("HR");
        Function Marketing_Function = new Function("Marketing");
        Function Business_Analyst_Function = new Function("Business Analyst");
        Function Resource_Supplier_Function = new Function("Resource Supplier");

        functionRepo.saveAll(Arrays.asList(COO_Function,HR_Function,Marketing_Function,Business_Analyst_Function,Resource_Supplier_Function));

        Geography Geography_India = new Geography("India");
        Geography Geography_UK = new Geography("UK");
        Geography Geography_Australia = new Geography("Australia");
        Geography Geography_Poland = new Geography("Poland");

        geographyRepo.saveAll(Arrays.asList(Geography_India,Geography_UK,Geography_Australia,Geography_Poland));

        Company TCS_Global = new Company("TCS Global");
        companyRepo.save(TCS_Global);

        shrikant.setCompany(TCS_Global);
        X.setCompany(TCS_Global);
        U.setCompany(TCS_Global);
        Y.setCompany(TCS_Global);

        shrikant.setFunction(COO_Function);
        X.setFunction(HR_Function);
        U.setFunction(Marketing_Function);
        Y.setFunction(Business_Analyst_Function );

        shrikant.setGeography(Geography_India);
        X.setGeography(Geography_UK);
        U.setGeography(Geography_Australia);
        Y.setGeography(Geography_Poland);

        shrikant.setEmpBusinessUnitList(Arrays.asList(COO_Emp_BusinessUnit));

        employeeRepo.saveAll(Arrays.asList(shrikant,X,U,Y));

        employeeProcessAreaRepo.saveAll(X_IPAndE_Emp_Process_Areas);

        employeeProcessAreaRepo.saveAll(Y_IPAndE_Emp_Process_Areas);

        employeeProcessAreaRepo.saveAll(U_IPAndE_Emp_Process_Areas);

//        remarksRepo.save(X_IPAndE_Remark1);

        remarksRepo.save(Y_IPAndE_Remark1);

        remarksRepo.save(U_IPAndE_Remark1);

        additionalDocumentRepo.save(X_IPAndE_Additional_document1);

        additionalDocumentRepo.save(Y_IPAndE_Additional_document1);


        additionalDocumentRepo.save(U_IPAndE_Additional_document1);

//        certificateRepo.save(X_IPAndE_Certificate);
//
//        certificateRepo.save(Y_IPAndE_Certificate);
//
//
//        certificateRepo.save(U_IPAndE_Certificate);

        employeeCertificationTaskDetailsRepo.save(X_IPAndE_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(X_IPAndE_SubmittedDetails);

        employeeCertificationTaskDetailsRepo.save(Y_IPAndE_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(Y_IPAndE_SubmittedDetails);

        employeeCertificationTaskDetailsRepo.save(U_IPAndE_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(U_IPAndE_SubmittedDetails);

//        certificateRepo.saveAll(Arrays.asList(X_IPAndE_Certificate,Y_IPAndE_Certificate,U_IPAndE_Certificate));

        additionalDocumentRepo.saveAll(Arrays.asList(X_IPAndE_Additional_document1,Y_IPAndE_Additional_document1,U_IPAndE_Additional_document1));

        employeeCertificationTaskRepo.save(X_IPAndE_Task);

        employeeProcessAreaRepo.saveAll(X_IPAndE_Emp_Process_Areas);

        employeeCertificationTaskRepo.save(Y_IPAndE_Task);


        employeeProcessAreaRepo.saveAll(Y_IPAndE_Emp_Process_Areas);

        employeeCertificationTaskRepo.save(U_IPAndE_Task);

        employeeCertificationTaskDetailsRepo.saveAll(
                Arrays.asList(X_IPAndE_SavedDETAILS,Y_IPAndE_SavedDETAILS,U_IPAndE_SavedDETAILS,
                        X_IPAndE_SubmittedDetails,Y_IPAndE_SubmittedDetails,U_IPAndE_SubmittedDetails));


        X.setEmpBusinessUnitList(Arrays.asList(X_Emp_BusinessUnit));
        U.setEmpBusinessUnitList(Arrays.asList(U_Emp_BusinessUnit));
        Y.setEmpBusinessUnitList(Arrays.asList(Y_Emp_BusinessUnit));
        employeeRepo.saveAll(Arrays.asList(X,U,Y));



        Employee A = new Employee("1100114","Divyanshu","123");
        Employee B = new Employee("1100115","Mukesh","123");
        Employee C = new Employee("1100116","Mayank","123");
        employeeRepo.saveAll(Arrays.asList(A,B,C));

        BusinessUnit CL_FSCI = new BusinessUnit("CL-FSCI","Cluster");
        BusinessUnit IS_FSCI_UK_2_Parent = new BusinessUnit("IS_FSCI_UK_2_Parent","National Place");
        BusinessUnit IS_FSCI_UK_23_Group3 = new BusinessUnit("IS_FSCI_UK_23_Group3","WireWorks Solution");

        businessUnitRepo.saveAll(Arrays.asList(CL_FSCI,IS_FSCI_UK_2_Parent,IS_FSCI_UK_23_Group3));

        EmpBusinessUnit A_Emp_BusinessUnit = new EmpBusinessUnit(CL_FSCI,"BG Head",new ArrayList<>());
        EmpBusinessUnit B_Emp_BusinessUnit = new EmpBusinessUnit(IS_FSCI_UK_2_Parent, "ISU Head",new ArrayList<>());
        EmpBusinessUnit C_Emp_BusinessUnit = new EmpBusinessUnit(IS_FSCI_UK_23_Group3,"Sub-ISU Head",new ArrayList<>());

        A_Emp_BusinessUnit.setBusinessUnit(CL_FSCI);
        A_Emp_BusinessUnit.setParentBusinessUnit(COO_Emp_BusinessUnit);
        A_Emp_BusinessUnit.setReportingToEmpId(shrikant.getEmpId());
        A_Emp_BusinessUnit.setEmpId(A.getEmpId());


        B_Emp_BusinessUnit.setParentBusinessUnit(A_Emp_BusinessUnit);
        B_Emp_BusinessUnit.setReportingToEmpId(A.getEmpId());
        B_Emp_BusinessUnit.setEmpId(B.getEmpId());

        C_Emp_BusinessUnit.setParentBusinessUnit(B_Emp_BusinessUnit);
        C_Emp_BusinessUnit.setReportingToEmpId(B.getEmpId());
        C_Emp_BusinessUnit.setEmpId(C.getEmpId());

        employeeBusinessUnitRepo.saveAll(Arrays.asList(A_Emp_BusinessUnit,B_Emp_BusinessUnit,C_Emp_BusinessUnit));

        EmpProcess A_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
        EmpProcess A_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
        EmpProcess A_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
        EmpProcess A_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
        EmpProcess A_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");

        final List<EmpProcess> A_CC_EmpProcessList1 = Arrays.asList(A_CC_EmpProcess1, A_CC_EmpProcess2);
        final List<EmpProcess> A_CC_EmpProcessList2 = Arrays.asList(A_CC_EmpProcess3, A_CC_EmpProcess4, A_CC_EmpProcess5);

        EmpProcess B_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
        EmpProcess B_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
        EmpProcess B_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
        EmpProcess B_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
        EmpProcess B_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");

        final List<EmpProcess> B_CC_EmpProcessList1 = Arrays.asList(B_CC_EmpProcess1, B_CC_EmpProcess2);
        final List<EmpProcess> B_CC_EmpProcessList2 = Arrays.asList(B_CC_EmpProcess3,B_CC_EmpProcess4, B_CC_EmpProcess5);

        EmpProcess C_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
        EmpProcess C_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
        EmpProcess C_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
        EmpProcess C_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
        EmpProcess C_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");

        final List<EmpProcess> C_CC_EmpProcessList1 = Arrays.asList(C_CC_EmpProcess1, C_CC_EmpProcess2);
        final List<EmpProcess> C_CC_EmpProcessList2 = Arrays.asList(C_CC_EmpProcess3,C_CC_EmpProcess4, C_CC_EmpProcess5);

        EmpProcessArea A_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,A_CC_EmpProcessList1);
        EmpProcessArea A_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,A_CC_EmpProcessList2);

        final List<EmpProcessArea> A_CC_Emp_Process_Areas = Arrays.asList(A_CC_Emp_Process_Area1,A_CC_Emp_Process_Area2);

        employeeProcessAreaRepo.saveAll(A_CC_Emp_Process_Areas);

        EmpProcessArea B_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,B_CC_EmpProcessList1);
        EmpProcessArea B_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,B_CC_EmpProcessList2);

        final List<EmpProcessArea> B_CC_Emp_Process_Areas = Arrays.asList(B_CC_Emp_Process_Area1,B_CC_Emp_Process_Area2);

        employeeProcessAreaRepo.saveAll(B_CC_Emp_Process_Areas);

        EmpProcessArea C_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,C_CC_EmpProcessList1);
        EmpProcessArea C_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,C_CC_EmpProcessList2);

        final List<EmpProcessArea> C_CC_Emp_Process_Areas = Arrays.asList(C_CC_Emp_Process_Area1,C_CC_Emp_Process_Area2);

        employeeProcessAreaRepo.saveAll(C_CC_Emp_Process_Areas);


        AdditionalDocument A_CC_Additional_document1 = new AdditionalDocument("A'sFILE1","#","descritpion");
        additionalDocumentRepo.save(A_CC_Additional_document1);

        AdditionalDocument B_CC_Additional_document1 = new AdditionalDocument("B'sFILE1","#","descritpion");
        additionalDocumentRepo.save(B_CC_Additional_document1);

        AdditionalDocument C_CC_Additional_document1 = new AdditionalDocument("C'sFILE1","#","descritpion");
        additionalDocumentRepo.save(C_CC_Additional_document1);


        EmpCertificateTaskDetails A_CC_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        A_CC_SavedDETAILS.setEmpProcessAreas(A_CC_Emp_Process_Areas);
        A_CC_SavedDETAILS.setRemarks(null);
        A_CC_SavedDETAILS.setAdditionalDocuments(Arrays.asList(A_CC_Additional_document1));
        A_CC_SavedDETAILS.setCertificate(null);

        EmpCertificateTaskDetails A_CC_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        A_CC_SubmittedDetails.setEmpProcessAreas(null);
        A_CC_SubmittedDetails.setRemarks(null);
        A_CC_SubmittedDetails.setAdditionalDocuments(null);
        A_CC_SubmittedDetails.setCertificate(null);
        A_CC_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(A_CC_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(A_CC_SubmittedDetails);

        EmpCertificateTaskDetails B_CC_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        B_CC_SavedDETAILS.setEmpProcessAreas(B_CC_Emp_Process_Areas);
        B_CC_SavedDETAILS.setRemarks(null);
        B_CC_SavedDETAILS.setAdditionalDocuments(Arrays.asList(B_CC_Additional_document1));
        B_CC_SavedDETAILS.setCertificate(null);

        EmpCertificateTaskDetails B_CC_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        B_CC_SubmittedDetails.setEmpProcessAreas(null);
        B_CC_SubmittedDetails.setRemarks(null);
        B_CC_SubmittedDetails.setAdditionalDocuments(null);
        B_CC_SubmittedDetails.setCertificate(null);
        B_CC_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(B_CC_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(B_CC_SubmittedDetails);

        EmpCertificateTaskDetails C_CC_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        C_CC_SavedDETAILS.setEmpProcessAreas(C_CC_Emp_Process_Areas);
        C_CC_SavedDETAILS.setRemarks(null);
        C_CC_SavedDETAILS.setAdditionalDocuments(Arrays.asList(C_CC_Additional_document1));
        C_CC_SavedDETAILS.setCertificate(null);

        EmpCertificateTaskDetails C_CC_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        C_CC_SubmittedDetails.setEmpProcessAreas(null);
        C_CC_SubmittedDetails.setRemarks(null);
        C_CC_SubmittedDetails.setAdditionalDocuments(null);
        C_CC_SubmittedDetails.setCertificate(null);
        C_CC_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(C_CC_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(C_CC_SubmittedDetails);
//
//        X_IPAndE_Certificate.setEmpCertificationTaskDetailsId(X_IPAndE_SavedDETAILS.getId());
//        B_IPAndE_Certificate.setEmpCertificationTaskDetailsId(Y_IPAndE_SavedDETAILS.getId());
//        U_IPAndE_Certificate.setEmpCertificationTaskDetailsId(U_IPAndE_SavedDETAILS.getId());
//
//        certificateRepo.saveAll(Arrays.asList(X_IPAndE_Certificate,Y_IPAndE_Certificate,U_IPAndE_Certificate));


        A_CC_Additional_document1.setEmpCertificationTaskDetailsId(A_CC_SavedDETAILS.getId());
        B_CC_Additional_document1.setEmpCertificationTaskDetailsId(B_CC_SavedDETAILS.getId());
        C_CC_Additional_document1.setEmpCertificationTaskDetailsId(C_CC_SavedDETAILS.getId());

        additionalDocumentRepo.saveAll(Arrays.asList(A_CC_Additional_document1,B_CC_Additional_document1,C_CC_Additional_document1));

        EmpCertTask A_CC_Task = new EmpCertTask(LocalDate.of(2021,6,1),A.getEmpId(),"Pending", A_CC_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        A_CC_Task.setCertTask(contractualCompliance);
        A_CC_Task.setEmpBusinessUnit(A_Emp_BusinessUnit);
        A_CC_Task.setCertifierName(A.getName());
        A_CC_Task.setEmployeeId(A.getEmpId());
        A_CC_Task.setSavedDetails(A_CC_SavedDETAILS);
        A_CC_Task.setSubmittedDetails(A_CC_SubmittedDetails);
        employeeCertificationTaskRepo.save(A_CC_Task);


        setCertificationTaskIdInEmpProcessArea(A_CC_Emp_Process_Areas,A_CC_Task.getId());


        EmpCertTask B_CC_Task = new EmpCertTask(LocalDate.of(2021,6,1),B.getEmpId(),"Pending", B_CC_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        B_CC_Task.setCertTask(contractualCompliance);
        B_CC_Task.setEmpBusinessUnit(B_Emp_BusinessUnit);
        B_CC_Task.setCertifierName(B.getName());
        B_CC_Task.setEmployeeId(B.getEmpId());
        B_CC_Task.setSavedDetails(B_CC_SavedDETAILS);
        B_CC_Task.setSubmittedDetails(B_CC_SubmittedDetails);
        employeeCertificationTaskRepo.save(B_CC_Task);


        setCertificationTaskIdInEmpProcessArea(B_CC_Emp_Process_Areas,B_CC_Task.getId());


        EmpCertTask C_CC_Task = new EmpCertTask(LocalDate.of(2021,6,1),C.getEmpId(),"Pending", B_CC_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        C_CC_Task.setCertTask(contractualCompliance);
        C_CC_Task.setEmpBusinessUnit(C_Emp_BusinessUnit);
        C_CC_Task.setCertifierName(C.getName());
        C_CC_Task.setEmployeeId(C.getEmpId());
        C_CC_Task.setSavedDetails(C_CC_SavedDETAILS);
        C_CC_Task.setSubmittedDetails(C_CC_SubmittedDetails);
        employeeCertificationTaskRepo.save(C_CC_Task);


        setCertificationTaskIdInEmpProcessArea(C_CC_Emp_Process_Areas,C_CC_Task.getId());

        employeeProcessAreaRepo.saveAll(A_CC_Emp_Process_Areas);
        employeeProcessAreaRepo.saveAll(B_CC_Emp_Process_Areas);
        employeeProcessAreaRepo.saveAll(C_CC_Emp_Process_Areas);

        A_CC_SavedDETAILS.setEmpCertTaskId(A_CC_Task.getId());
        B_CC_SavedDETAILS.setEmpCertTaskId(B_CC_Task.getId());
        C_CC_SavedDETAILS.setEmpCertTaskId(C_CC_Task.getId());

        A_CC_SubmittedDetails.setEmpCertTaskId(A_CC_Task.getId());
        B_CC_SubmittedDetails.setEmpCertTaskId(A_CC_Task.getId());
        C_CC_SubmittedDetails.setEmpCertTaskId(A_CC_Task.getId());

        employeeCertificationTaskDetailsRepo.saveAll(
                Arrays.asList(A_CC_SavedDETAILS,B_CC_SavedDETAILS,C_CC_SavedDETAILS,
                        A_CC_SubmittedDetails,B_CC_SubmittedDetails,C_CC_SubmittedDetails));

        A.setCompany(TCS_Global);
        B.setCompany(TCS_Global);
        C.setCompany(TCS_Global);

        A.setFunction(HR_Function);
        B.setFunction(Marketing_Function);
        C.setFunction(Business_Analyst_Function );

        A.setGeography(Geography_UK);
        B.setGeography(Geography_Australia);
        C.setGeography(Geography_Poland);

        employeeRepo.saveAll(Arrays.asList(A,B,C));
        employeeBusinessUnitRepo.saveAll(Arrays.asList(A_Emp_BusinessUnit,B_Emp_BusinessUnit,C_Emp_BusinessUnit));


        A.setEmpBusinessUnitList(Arrays.asList(A_Emp_BusinessUnit));
        B.setEmpBusinessUnitList(Arrays.asList(B_Emp_BusinessUnit));
        C.setEmpBusinessUnitList(Arrays.asList(C_Emp_BusinessUnit));
        employeeRepo.saveAll(Arrays.asList(A,B,C));


        //----------------------------3rd Hierarchy

        Employee D = new Employee("1100117","Kirti","123");
        Employee E = new Employee("1100118","Manav","123");
        Employee F = new Employee("1100119","Gautam","123");
        employeeRepo.saveAll(Arrays.asList(D,E,F));


        BusinessUnit CL_CCFSI = new BusinessUnit("CL_CCFSI","Cluster");
        BusinessUnit CL_CCFSI_UK_2_Parent = new BusinessUnit("CL_CCFSI_UK_2_Parent","National Place");
        BusinessUnit CL_CCFSI_UK_23_Group3 = new BusinessUnit("CL_CCFSI_UK_23_Group3","WireWorks Solution");


        businessUnitRepo.saveAll(Arrays.asList(CL_CCFSI,CL_CCFSI_UK_2_Parent,CL_CCFSI_UK_23_Group3));

        EmpBusinessUnit D_Emp_BusinessUnit = new EmpBusinessUnit(CL_CCFSI,"B&TS Head",new ArrayList<>());
        EmpBusinessUnit E_Emp_BusinessUnit = new EmpBusinessUnit(CL_CCFSI_UK_2_Parent, "SP Head",new ArrayList<>());
        EmpBusinessUnit F_Emp_BusinessUnit = new EmpBusinessUnit(CL_CCFSI_UK_23_Group3,"Sub-SP Head",new ArrayList<>());

        D_Emp_BusinessUnit.setBusinessUnit(CL_CCFSI);
        D_Emp_BusinessUnit.setParentBusinessUnit(COO_Emp_BusinessUnit);
        D_Emp_BusinessUnit.setReportingToEmpId(shrikant.getEmpId());
        D_Emp_BusinessUnit.setEmpId(D.getEmpId());


        E_Emp_BusinessUnit.setParentBusinessUnit(D_Emp_BusinessUnit);
        E_Emp_BusinessUnit.setReportingToEmpId(D.getEmpId());
        E_Emp_BusinessUnit.setEmpId(E.getEmpId());

        F_Emp_BusinessUnit.setParentBusinessUnit(E_Emp_BusinessUnit);
        F_Emp_BusinessUnit.setReportingToEmpId(E.getEmpId());
        F_Emp_BusinessUnit.setEmpId(F.getEmpId());

        employeeBusinessUnitRepo.saveAll(Arrays.asList(D_Emp_BusinessUnit,E_Emp_BusinessUnit,F_Emp_BusinessUnit));

        EmpProcess D_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
        EmpProcess D_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
        EmpProcess D_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
        EmpProcess D_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
        EmpProcess D_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");

        final List<EmpProcess> D_CC_EmpProcessList1 = Arrays.asList(D_CC_EmpProcess1, D_CC_EmpProcess2);
        final List<EmpProcess> D_CC_EmpProcessList2 = Arrays.asList(D_CC_EmpProcess3, D_CC_EmpProcess4, D_CC_EmpProcess5);

        EmpProcess E_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
        EmpProcess E_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
        EmpProcess E_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
        EmpProcess E_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
        EmpProcess E_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");

        final List<EmpProcess> E_CC_EmpProcessList1 = Arrays.asList(E_CC_EmpProcess1, E_CC_EmpProcess2);
        final List<EmpProcess> E_CC_EmpProcessList2 = Arrays.asList(E_CC_EmpProcess3,E_CC_EmpProcess4, E_CC_EmpProcess5);

        EmpProcess F_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
        EmpProcess F_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
        EmpProcess F_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
        EmpProcess F_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
        EmpProcess F_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");

        final List<EmpProcess> F_CC_EmpProcessList1 = Arrays.asList(F_CC_EmpProcess1, F_CC_EmpProcess2);
        final List<EmpProcess> F_CC_EmpProcessList2 = Arrays.asList(F_CC_EmpProcess3,F_CC_EmpProcess4, F_CC_EmpProcess5);


        EmpProcessArea D_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,D_CC_EmpProcessList1);
        EmpProcessArea D_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,D_CC_EmpProcessList2);

        final List<EmpProcessArea> D_CC_Emp_Process_Areas = Arrays.asList(D_CC_Emp_Process_Area1,D_CC_Emp_Process_Area2);

        employeeProcessAreaRepo.saveAll(D_CC_Emp_Process_Areas);

        EmpProcessArea E_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,E_CC_EmpProcessList1);
        EmpProcessArea E_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,E_CC_EmpProcessList2);

        final List<EmpProcessArea> E_CC_Emp_Process_Areas = Arrays.asList(E_CC_Emp_Process_Area1,E_CC_Emp_Process_Area2);

        employeeProcessAreaRepo.saveAll(E_CC_Emp_Process_Areas);

        EmpProcessArea F_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,F_CC_EmpProcessList1);
        EmpProcessArea F_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,F_CC_EmpProcessList2);

        final List<EmpProcessArea> F_CC_Emp_Process_Areas = Arrays.asList(F_CC_Emp_Process_Area1,F_CC_Emp_Process_Area2);

        employeeProcessAreaRepo.saveAll(F_CC_Emp_Process_Areas);


        AdditionalDocument D_CC_Additional_document1 = new AdditionalDocument("D'sFILE1","#","descritpion");
        additionalDocumentRepo.save(D_CC_Additional_document1);

        AdditionalDocument E_CC_Additional_document1 = new AdditionalDocument("E'sFILE1","#","descritpion");
        additionalDocumentRepo.save(E_CC_Additional_document1);

        AdditionalDocument F_CC_Additional_document1 = new AdditionalDocument("F'sFILE1","#","descritpion");
        additionalDocumentRepo.save(F_CC_Additional_document1);


        EmpCertificateTaskDetails D_CC_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        D_CC_SavedDETAILS.setEmpProcessAreas(D_CC_Emp_Process_Areas);
        D_CC_SavedDETAILS.setRemarks(null);
        D_CC_SavedDETAILS.setAdditionalDocuments(Arrays.asList(D_CC_Additional_document1));
        D_CC_SavedDETAILS.setCertificate(null);

        EmpCertificateTaskDetails D_CC_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        D_CC_SubmittedDetails.setEmpProcessAreas(null);
        D_CC_SubmittedDetails.setRemarks(null);
        D_CC_SubmittedDetails.setAdditionalDocuments(null);
        D_CC_SubmittedDetails.setCertificate(null);
        D_CC_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(D_CC_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(D_CC_SubmittedDetails);


        EmpCertificateTaskDetails E_CC_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        E_CC_SavedDETAILS.setEmpProcessAreas(E_CC_Emp_Process_Areas);
        E_CC_SavedDETAILS.setRemarks(null);
        E_CC_SavedDETAILS.setAdditionalDocuments(Arrays.asList(E_CC_Additional_document1));
        E_CC_SavedDETAILS.setCertificate(null);

        EmpCertificateTaskDetails E_CC_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        E_CC_SubmittedDetails.setEmpProcessAreas(null);
        E_CC_SubmittedDetails.setRemarks(null);
        E_CC_SubmittedDetails.setAdditionalDocuments(null);
        E_CC_SubmittedDetails.setCertificate(null);
        E_CC_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(E_CC_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(E_CC_SubmittedDetails);

        EmpCertificateTaskDetails F_CC_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        F_CC_SavedDETAILS.setEmpProcessAreas(F_CC_Emp_Process_Areas);
        F_CC_SavedDETAILS.setRemarks(null);
        F_CC_SavedDETAILS.setAdditionalDocuments(Arrays.asList(F_CC_Additional_document1));
        F_CC_SavedDETAILS.setCertificate(null);

        EmpCertificateTaskDetails F_CC_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        F_CC_SubmittedDetails.setEmpProcessAreas(null);
        F_CC_SubmittedDetails.setRemarks(null);
        F_CC_SubmittedDetails.setAdditionalDocuments(null);
        F_CC_SubmittedDetails.setCertificate(null);
        F_CC_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(F_CC_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(F_CC_SubmittedDetails);

        D_CC_Additional_document1.setEmpCertificationTaskDetailsId(D_CC_SavedDETAILS.getId());
        E_CC_Additional_document1.setEmpCertificationTaskDetailsId(E_CC_SavedDETAILS.getId());
        F_CC_Additional_document1.setEmpCertificationTaskDetailsId(F_CC_SavedDETAILS.getId());

        additionalDocumentRepo.saveAll(Arrays.asList(D_CC_Additional_document1,E_CC_Additional_document1,F_CC_Additional_document1));

        EmpCertTask D_CC_Task = new EmpCertTask(LocalDate.of(2021,6,1),D.getEmpId(),"Pending", D_CC_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        D_CC_Task.setCertTask(contractualCompliance);
        D_CC_Task.setEmpBusinessUnit(D_Emp_BusinessUnit);
        D_CC_Task.setCertifierName(D.getName());
        D_CC_Task.setEmployeeId(D.getEmpId());
        D_CC_Task.setSavedDetails(D_CC_SavedDETAILS);
        D_CC_Task.setSubmittedDetails(D_CC_SubmittedDetails);
        employeeCertificationTaskRepo.save(D_CC_Task);


        setCertificationTaskIdInEmpProcessArea(D_CC_Emp_Process_Areas,D_CC_Task.getId());


        EmpCertTask E_CC_Task = new EmpCertTask(LocalDate.of(2021,6,1),E.getEmpId(),"Pending", E_CC_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        E_CC_Task.setCertTask(contractualCompliance);
        E_CC_Task.setEmpBusinessUnit(E_Emp_BusinessUnit);
        E_CC_Task.setCertifierName(E.getName());
        E_CC_Task.setEmployeeId(E.getEmpId());
        E_CC_Task.setSavedDetails(E_CC_SavedDETAILS);
        E_CC_Task.setSubmittedDetails(E_CC_SubmittedDetails);
        employeeCertificationTaskRepo.save(E_CC_Task);


        setCertificationTaskIdInEmpProcessArea(E_CC_Emp_Process_Areas,E_CC_Task.getId());


        EmpCertTask F_CC_Task = new EmpCertTask(LocalDate.of(2021,6,1),F.getEmpId(),"Pending", F_CC_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        F_CC_Task.setCertTask(contractualCompliance);
        F_CC_Task.setEmpBusinessUnit(F_Emp_BusinessUnit);
        F_CC_Task.setCertifierName(F.getName());
        F_CC_Task.setEmployeeId(F.getEmpId());
        F_CC_Task.setSavedDetails(F_CC_SavedDETAILS);
        F_CC_Task.setSubmittedDetails(F_CC_SubmittedDetails);
        employeeCertificationTaskRepo.save(F_CC_Task);


        setCertificationTaskIdInEmpProcessArea(F_CC_Emp_Process_Areas,F_CC_Task.getId());

        employeeProcessAreaRepo.saveAll(D_CC_Emp_Process_Areas);
        employeeProcessAreaRepo.saveAll(E_CC_Emp_Process_Areas);
        employeeProcessAreaRepo.saveAll(F_CC_Emp_Process_Areas);


        D_CC_SavedDETAILS.setEmpCertTaskId(D_CC_Task.getId());
        E_CC_SavedDETAILS.setEmpCertTaskId(E_CC_Task.getId());
        F_CC_SavedDETAILS.setEmpCertTaskId(F_CC_Task.getId());

        D_CC_SubmittedDetails.setEmpCertTaskId(D_CC_Task.getId());
        E_CC_SubmittedDetails.setEmpCertTaskId(E_CC_Task.getId());
        F_CC_SubmittedDetails.setEmpCertTaskId(F_CC_Task.getId());

        employeeCertificationTaskDetailsRepo.saveAll(
                Arrays.asList(D_CC_SavedDETAILS,E_CC_SavedDETAILS,F_CC_SavedDETAILS,
                        D_CC_SubmittedDetails,E_CC_SubmittedDetails,F_CC_SubmittedDetails));


        D.setCompany(TCS_Global);
        E.setCompany(TCS_Global);
        F.setCompany(TCS_Global);

        D.setFunction(HR_Function);
        E.setFunction(Marketing_Function);
        F.setFunction(Business_Analyst_Function );

        D.setGeography(Geography_UK);
        E.setGeography(Geography_Australia);
        F.setGeography(Geography_Poland);

        employeeRepo.saveAll(Arrays.asList(D,E,F));
        employeeBusinessUnitRepo.saveAll(Arrays.asList(D_Emp_BusinessUnit,E_Emp_BusinessUnit,F_Emp_BusinessUnit));


        D.setEmpBusinessUnitList(Arrays.asList(D_Emp_BusinessUnit));
        E.setEmpBusinessUnitList(Arrays.asList(E_Emp_BusinessUnit));
        F.setEmpBusinessUnitList(Arrays.asList(F_Emp_BusinessUnit));
        employeeRepo.saveAll(Arrays.asList(D,E,F));

        //------------------Recurring Hierarchy

        Employee G = new Employee("11001110","Saurabh","123");
        employeeRepo.save(G);

        BusinessUnit CL_DFSI = new BusinessUnit("CL_DFSI","Cluster");
        BusinessUnit CL_DFSI_UK_2_Parent = new BusinessUnit("CL_CCFSI_UK_2_Parent","National Place");
        BusinessUnit CL_DFSI_UK_23_Group3 = new BusinessUnit("CL_CCFSI_UK_23_Group3","WireWorks Solution");

        businessUnitRepo.saveAll(Arrays.asList(CL_DFSI,CL_DFSI_UK_2_Parent,CL_DFSI_UK_23_Group3));

        EmpBusinessUnit G_Level2_Emp_BusinessUnit = new EmpBusinessUnit(CL_DFSI,"B&TS Head",new ArrayList<>());
        EmpBusinessUnit G_Level3_Emp_BusinessUnit = new EmpBusinessUnit(CL_DFSI_UK_2_Parent, "SP Head",new ArrayList<>());
        EmpBusinessUnit G_Level4_Emp_BusinessUnit = new EmpBusinessUnit(CL_DFSI_UK_23_Group3,"Sub-SP Head",new ArrayList<>());

        G_Level2_Emp_BusinessUnit.setBusinessUnit(CL_DFSI);
        G_Level2_Emp_BusinessUnit.setParentBusinessUnit(COO_Emp_BusinessUnit);
        G_Level2_Emp_BusinessUnit.setReportingToEmpId(shrikant.getEmpId());
        G_Level2_Emp_BusinessUnit.setEmpId(G.getEmpId());


        G_Level3_Emp_BusinessUnit.setParentBusinessUnit(G_Level2_Emp_BusinessUnit);
        G_Level3_Emp_BusinessUnit.setReportingToEmpId(G.getEmpId());
        G_Level3_Emp_BusinessUnit.setEmpId(G.getEmpId());

        G_Level4_Emp_BusinessUnit.setParentBusinessUnit(G_Level3_Emp_BusinessUnit);
        G_Level4_Emp_BusinessUnit.setReportingToEmpId(G.getEmpId());
        G_Level4_Emp_BusinessUnit.setEmpId(G.getEmpId());

        employeeBusinessUnitRepo.saveAll(Arrays.asList(G_Level2_Emp_BusinessUnit,G_Level3_Emp_BusinessUnit,G_Level4_Emp_BusinessUnit));


        EmpProcess G_Level2_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
        EmpProcess G_Level2_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
        EmpProcess G_Level2_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
        EmpProcess G_Level2_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
        EmpProcess G_Level2_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");

        final List<EmpProcess> G_Level2_CC_EmpProcessList1 = Arrays.asList(G_Level2_CC_EmpProcess1, G_Level2_CC_EmpProcess2);
        final List<EmpProcess> G_Level2_CC_EmpProcessList2 = Arrays.asList(G_Level2_CC_EmpProcess3, G_Level2_CC_EmpProcess4, G_Level2_CC_EmpProcess5);

        EmpProcess G_Level3_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
        EmpProcess G_Level3_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
        EmpProcess G_Level3_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
        EmpProcess G_Level3_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
        EmpProcess G_Level3_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");

        final List<EmpProcess> G_Level3_CC_EmpProcessList1 = Arrays.asList(G_Level3_CC_EmpProcess2, G_Level3_CC_EmpProcess1);
        final List<EmpProcess> G_Level3_CC_EmpProcessList2 = Arrays.asList(G_Level3_CC_EmpProcess3,G_Level3_CC_EmpProcess4, G_Level3_CC_EmpProcess5);

        EmpProcess G_Level4_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
        EmpProcess G_Level4_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
        EmpProcess G_Level4_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
        EmpProcess G_Level4_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
        EmpProcess G_Level4_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");

        final List<EmpProcess> G_Level4_CC_EmpProcessList1 = Arrays.asList(G_Level4_CC_EmpProcess1, G_Level4_CC_EmpProcess2);
        final List<EmpProcess> G_Level4_CC_EmpProcessList2 = Arrays.asList(G_Level4_CC_EmpProcess3,G_Level4_CC_EmpProcess4, G_Level4_CC_EmpProcess5);



        EmpProcessArea G_Level2_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,G_Level2_CC_EmpProcessList1);
        EmpProcessArea G_Level2_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,G_Level2_CC_EmpProcessList2);

        final List<EmpProcessArea> G_Level2_CC_Emp_Process_Areas = Arrays.asList(G_Level2_CC_Emp_Process_Area1,G_Level2_CC_Emp_Process_Area2);

        employeeProcessAreaRepo.saveAll(G_Level2_CC_Emp_Process_Areas);

        EmpProcessArea G_Level3_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,G_Level3_CC_EmpProcessList1);
        EmpProcessArea G_Level3_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,G_Level3_CC_EmpProcessList2);

        final List<EmpProcessArea> G_Level3_CC_Emp_Process_Areas = Arrays.asList(G_Level3_CC_Emp_Process_Area1,G_Level3_CC_Emp_Process_Area2);

        employeeProcessAreaRepo.saveAll(G_Level3_CC_Emp_Process_Areas);

        EmpProcessArea G_Level4_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,G_Level4_CC_EmpProcessList1);
        EmpProcessArea G_Level4_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,G_Level4_CC_EmpProcessList2);

        final List<EmpProcessArea> G_Level4_CC_Emp_Process_Areas = Arrays.asList(G_Level4_CC_Emp_Process_Area1,G_Level4_CC_Emp_Process_Area2);

        employeeProcessAreaRepo.saveAll(G_Level4_CC_Emp_Process_Areas);


        AdditionalDocument G_Level2_CC_Additional_document1 = new AdditionalDocument("GLevel2'sFILE1","#","descritpion");
        additionalDocumentRepo.save(G_Level2_CC_Additional_document1);

        AdditionalDocument G_Level3_CC_Additional_document1 = new AdditionalDocument("GLevel3FILE1","#","descritpion");
        additionalDocumentRepo.save(G_Level3_CC_Additional_document1);

        AdditionalDocument G_Level4_CC_Additional_document1 = new AdditionalDocument("GLevel4FILE1","#","descritpion");
        additionalDocumentRepo.save(G_Level4_CC_Additional_document1);



        EmpCertificateTaskDetails G_Level2_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        G_Level2_SavedDETAILS.setEmpProcessAreas(G_Level2_CC_Emp_Process_Areas);
        G_Level2_SavedDETAILS.setRemarks(null);
        G_Level2_SavedDETAILS.setAdditionalDocuments(Arrays.asList(G_Level2_CC_Additional_document1));
        G_Level2_SavedDETAILS.setCertificate(null);

        EmpCertificateTaskDetails G_Level2_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        G_Level2_SubmittedDetails.setEmpProcessAreas(null);
        G_Level2_SubmittedDetails.setRemarks(null);
        G_Level2_SubmittedDetails.setAdditionalDocuments(null);
        G_Level2_SubmittedDetails.setCertificate(null);
        G_Level2_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(G_Level2_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(G_Level2_SubmittedDetails);


        EmpCertificateTaskDetails G_Level3_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        G_Level3_SavedDETAILS.setEmpProcessAreas(G_Level3_CC_Emp_Process_Areas);
        G_Level3_SavedDETAILS.setRemarks(null);
        G_Level3_SavedDETAILS.setAdditionalDocuments(Arrays.asList(G_Level3_CC_Additional_document1));
        G_Level3_SavedDETAILS.setCertificate(null);

        EmpCertificateTaskDetails G_Level3_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        G_Level3_SubmittedDetails.setEmpProcessAreas(null);
        G_Level3_SubmittedDetails.setRemarks(null);
        G_Level3_SubmittedDetails.setAdditionalDocuments(null);
        G_Level3_SubmittedDetails.setCertificate(null);
        G_Level3_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(G_Level3_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(G_Level3_SubmittedDetails);

        EmpCertificateTaskDetails G_Level4_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
        G_Level4_SavedDETAILS.setEmpProcessAreas(G_Level4_CC_Emp_Process_Areas);
        G_Level4_SavedDETAILS.setRemarks(null);
        G_Level4_SavedDETAILS.setAdditionalDocuments(Arrays.asList(G_Level4_CC_Additional_document1));
        G_Level4_SavedDETAILS.setCertificate(null);

        EmpCertificateTaskDetails G_Level4_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
        G_Level4_SubmittedDetails.setEmpProcessAreas(null);
        G_Level4_SubmittedDetails.setRemarks(null);
        G_Level4_SubmittedDetails.setAdditionalDocuments(null);
        G_Level4_SubmittedDetails.setCertificate(null);
        G_Level4_SubmittedDetails.setCertificationStatus("Pending");

        employeeCertificationTaskDetailsRepo.save(G_Level4_SavedDETAILS);
        employeeCertificationTaskDetailsRepo.save(G_Level4_SubmittedDetails);


        G_Level2_CC_Additional_document1.setEmpCertificationTaskDetailsId(G_Level2_SavedDETAILS.getId());
        G_Level3_CC_Additional_document1.setEmpCertificationTaskDetailsId(G_Level3_SavedDETAILS.getId());
        G_Level4_CC_Additional_document1.setEmpCertificationTaskDetailsId(G_Level3_SavedDETAILS.getId());

        additionalDocumentRepo.saveAll(Arrays.asList(G_Level2_CC_Additional_document1,G_Level3_CC_Additional_document1,G_Level4_CC_Additional_document1));


        EmpCertTask G_Level2_Task = new EmpCertTask(LocalDate.of(2021,6,1),G.getEmpId(),"Pending", G_Level2_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        G_Level2_Task.setCertTask(contractualCompliance);
        G_Level2_Task.setEmpBusinessUnit(G_Level2_Emp_BusinessUnit);
        G_Level2_Task.setCertifierName(G.getName());
        G_Level2_Task.setEmployeeId(G.getEmpId());
        G_Level2_Task.setSavedDetails(G_Level2_SavedDETAILS);
        G_Level2_Task.setSubmittedDetails(G_Level2_SubmittedDetails);
        employeeCertificationTaskRepo.save(G_Level2_Task);


        setCertificationTaskIdInEmpProcessArea(G_Level2_CC_Emp_Process_Areas,G_Level2_Task.getId());


        EmpCertTask G_level3_Task = new EmpCertTask(LocalDate.of(2021,6,1),G.getEmpId(),"Pending", G_Level3_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        G_level3_Task.setCertTask(contractualCompliance);
        G_level3_Task.setEmpBusinessUnit(G_Level3_Emp_BusinessUnit);
        G_level3_Task.setCertifierName(G.getName());
        G_level3_Task.setEmployeeId(G.getEmpId());
        G_level3_Task.setSavedDetails(G_Level3_SavedDETAILS);
        G_level3_Task.setSubmittedDetails(G_Level3_SubmittedDetails);
        employeeCertificationTaskRepo.save(G_level3_Task);


        setCertificationTaskIdInEmpProcessArea(G_Level3_CC_Emp_Process_Areas,G_level3_Task.getId());


        EmpCertTask G_level4_Task = new EmpCertTask(LocalDate.of(2021,6,1),G.getEmpId(),"Pending", G_Level4_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);

        G_level4_Task.setCertTask(contractualCompliance);
        G_level4_Task.setEmpBusinessUnit(G_Level4_Emp_BusinessUnit);
        G_level4_Task.setCertifierName(G.getName());
        G_level4_Task.setEmployeeId(G.getEmpId());
        G_level4_Task.setSavedDetails(G_Level4_SavedDETAILS);
        G_level4_Task.setSubmittedDetails(G_Level4_SubmittedDetails);
        employeeCertificationTaskRepo.save(G_level4_Task);


        setCertificationTaskIdInEmpProcessArea(G_Level4_CC_Emp_Process_Areas,G_level4_Task.getId());

        employeeProcessAreaRepo.saveAll(G_Level2_CC_Emp_Process_Areas);
        employeeProcessAreaRepo.saveAll(G_Level3_CC_Emp_Process_Areas);
        employeeProcessAreaRepo.saveAll(G_Level4_CC_Emp_Process_Areas);

        G_Level2_SavedDETAILS.setEmpCertTaskId(G_Level2_Task.getId());
        G_Level3_SavedDETAILS.setEmpCertTaskId(G_level3_Task.getId());
        G_Level4_SavedDETAILS.setEmpCertTaskId(G_level4_Task.getId());

        G_Level2_SubmittedDetails.setEmpCertTaskId(G_Level2_Task.getId());
        G_Level3_SubmittedDetails.setEmpCertTaskId(G_level3_Task.getId());
        G_Level4_SubmittedDetails.setEmpCertTaskId(G_level4_Task.getId());

        employeeCertificationTaskDetailsRepo.saveAll(
                Arrays.asList(G_Level2_SavedDETAILS,G_Level3_SavedDETAILS,G_Level4_SavedDETAILS,
                        G_Level2_SubmittedDetails,G_Level3_SubmittedDetails,G_Level4_SubmittedDetails));

        G.setCompany(TCS_Global);
        G.setFunction(HR_Function);
        G.setGeography(Geography_UK);

        employeeRepo.saveAll(Arrays.asList(G));
        employeeBusinessUnitRepo.saveAll(Arrays.asList(G_Level2_Emp_BusinessUnit,G_Level3_Emp_BusinessUnit,G_Level4_Emp_BusinessUnit));

        G.setEmpBusinessUnitList(Arrays.asList(G_Level2_Emp_BusinessUnit,G_Level3_Emp_BusinessUnit,G_Level4_Emp_BusinessUnit));
        employeeRepo.saveAll(Arrays.asList(G));
    }

    private void setCertificationTaskIdInEmpProcessArea(List<EmpProcessArea> IPAndE_Emp_Process_Areas,Integer certificationTaskId) {
        for (EmpProcessArea areas : IPAndE_Emp_Process_Areas) {
            areas.setEmployeeCertificationTaskId(certificationTaskId);
        }
    }
}
