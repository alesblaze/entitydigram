package com.example.entityDiagram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EntityDiagramApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntityDiagramApplication.class, args);
	}

//	@Bean
//	public DbInit getDbInit() {
//		return new DbInit();
//	}
	@Bean
	public DbCode getDbInit() {
		return new DbCode();
	}

}
