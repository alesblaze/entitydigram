//package com.example.entityDiagram;
//
//import com.example.entityDiagram.Repo.*;
//import com.example.entityDiagram.entity.Employee;
//import com.example.entityDiagram.entity.businessunit.BusinessUnit;
//import com.example.entityDiagram.entity.businessunit.EmpBusinessUnit;
//import com.example.entityDiagram.entity.certificationtask.EmpCertTask;
//import com.example.entityDiagram.entity.certificationtaskdetails.AdditionalDocument;
//import com.example.entityDiagram.entity.certificationtaskdetails.EmpCertificateTaskDetails;
//import com.example.entityDiagram.entity.certificationtaskdetails.EmpProcess;
//import com.example.entityDiagram.entity.certificationtaskdetails.EmpProcessArea;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//public class DbCode2 {
//
//    @Autowired
//    private AdditionalDocumentRepo additionalDocumentRepo;
//    @Autowired
//    private AssetDetailsRepo assetDetailsRepo;
//    @Autowired
//    private EmployeeBusinessUnitRepo employeeBusinessUnitRepo;
//    @Autowired
//    private BusinessUnitRepo businessUnitRepo;
//    @Autowired
//    private CertificateRepo certificateRepo;
//    @Autowired
//    private CertificationTaskRepo certificationTaskRepo;
//    @Autowired
//    private EmployeeCertificationTaskDetailsRepo employeeCertificationTaskDetailsRepo;
//    @Autowired
//    private EmployeeCertificationTaskRepo employeeCertificationTaskRepo;
//    @Autowired
//    private EmployeeProcessAreaRepo employeeProcessAreaRepo;
//    @Autowired
//    private EmployeeProcessRepo employeeProcessRepo;
//    @Autowired
//    private EmployeeRepo employeeRepo;
//    @Autowired
//    private ProcessAreaRepo processAreaRepo;
//    @Autowired
//    private ProcessRepo processRepo;
//    @Autowired
//    private RemarksRepo remarksRepo;
//    @Autowired
//    private FunctionRepo functionRepo;
//    @Autowired
//    private GeographyRepo geographyRepo;
//    @Autowired
//    private CompanyRepo companyRepo;
//
//    public void main(String[] args) {
//
//        Employee G = new Employee("11001110","G","G...");
//
//        BusinessUnit CL_DFSI = new BusinessUnit("CL_DFSI","Cluster");
//        BusinessUnit CL_DFSI_UK_2_Parent = new BusinessUnit("CL_CCFSI_UK_2_Parent","National Place");
//        BusinessUnit CL_DFSI_UK_23_Group3 = new BusinessUnit("CL_CCFSI_UK_23_Group3","WireWorks Solution");
//
//        businessUnitRepo.saveAll(Arrays.asList(CL_DFSI,CL_DFSI_UK_2_Parent,CL_DFSI_UK_23_Group3));
//
//        EmpBusinessUnit G_Level2_Emp_BusinessUnit = new EmpBusinessUnit(CL_DFSI,"B&TS Head",new ArrayList<>());
//        EmpBusinessUnit G_Level3_Emp_BusinessUnit = new EmpBusinessUnit(CL_DFSI_UK_2_Parent, "SP Head",new ArrayList<>());
//        EmpBusinessUnit G_Level4_Emp_BusinessUnit = new EmpBusinessUnit(CL_DFSI_UK_23_Group3,"Sub-SP Head",new ArrayList<>());
//
//        G_Level2_Emp_BusinessUnit.setBusinessUnit(CL_DFSI);
//        G_Level2_Emp_BusinessUnit.setParentBusinessUnit(COO_Emp_BusinessUnit);
//        G_Level2_Emp_BusinessUnit.setReportingToEmpId(shrikant.getEmpId());
//        G_Level2_Emp_BusinessUnit.setEmpId(G.getEmpId());
//
//
//        G_Level3_Emp_BusinessUnit.setParentBusinessUnit(G_Level2_Emp_BusinessUnit);
//        G_Level3_Emp_BusinessUnit.setReportingToEmpId(G.getEmpId());
//        G_Level3_Emp_BusinessUnit.setEmpId(G.getEmpId());
//
//        G_Level4_Emp_BusinessUnit.setParentBusinessUnit(G_Level3_Emp_BusinessUnit);
//        G_Level4_Emp_BusinessUnit.setReportingToEmpId(G.getEmpId());
//        G_Level4_Emp_BusinessUnit.setEmpId(G.getEmpId());
//
//        employeeBusinessUnitRepo.saveAll(Arrays.asList(G_Level2_Emp_BusinessUnit,G_Level3_Emp_BusinessUnit,G_Level4_Emp_BusinessUnit));
//
//
//        EmpProcess G_Level2_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
//        EmpProcess G_Level2_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
//        EmpProcess G_Level2_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
//        EmpProcess G_Level2_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
//        EmpProcess G_Level2_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");
//
//        final List<EmpProcess> G_Level2_CC_EmpProcessList1 = Arrays.asList(G_Level2_CC_EmpProcess1, G_Level2_CC_EmpProcess2);
//        final List<EmpProcess> G_Level2_CC_EmpProcessList2 = Arrays.asList(G_Level2_CC_EmpProcess3, G_Level2_CC_EmpProcess4, G_Level2_CC_EmpProcess5);
//
//        EmpProcess G_Level3_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
//        EmpProcess G_Level3_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
//        EmpProcess G_Level3_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
//        EmpProcess G_Level3_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
//        EmpProcess G_Level3_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");
//
//        final List<EmpProcess> G_Level3_CC_EmpProcessList1 = Arrays.asList(G_Level3_CC_EmpProcess2, G_Level3_CC_EmpProcess1);
//        final List<EmpProcess> G_Level3_CC_EmpProcessList2 = Arrays.asList(G_Level3_CC_EmpProcess3,G_Level3_CC_EmpProcess4, G_Level3_CC_EmpProcess5);
//
//        EmpProcess G_Level4_CC_EmpProcess1 = new EmpProcess(ContractProcessArea1Process1,"No","High","Some Remarks");
//        EmpProcess G_Level4_CC_EmpProcess2 = new EmpProcess(ContractProcessArea1Process2,"No","High","Some Remarks");
//        EmpProcess G_Level4_CC_EmpProcess3 = new EmpProcess(ContractProcessArea2Process1,"No","High","Some Remarks");
//        EmpProcess G_Level4_CC_EmpProcess4 = new EmpProcess(ContractProcessArea2Process2,"No","High","Some Remarks");
//        EmpProcess G_Level4_CC_EmpProcess5 = new EmpProcess(ContractProcessArea2Process3,"No","High","Some Remarks");
//
//        final List<EmpProcess> G_Level4_CC_EmpProcessList1 = Arrays.asList(G_Level4_CC_EmpProcess1, G_Level4_CC_EmpProcess2);
//        final List<EmpProcess> G_Level4_CC_EmpProcessList2 = Arrays.asList(G_Level4_CC_EmpProcess3,G_Level4_CC_EmpProcess4, G_Level4_CC_EmpProcess5);
//
//
//
//        EmpProcessArea G_Level2_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,G_Level2_CC_EmpProcessList1);
//        EmpProcessArea G_Level2_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,G_Level2_CC_EmpProcessList2);
//
//        final List<EmpProcessArea> G_Level2_CC_Emp_Process_Areas = Arrays.asList(G_Level2_CC_Emp_Process_Area1,G_Level2_CC_Emp_Process_Area2);
//
//        employeeProcessAreaRepo.saveAll(G_Level2_CC_Emp_Process_Areas);
//
//        EmpProcessArea G_Level3_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,G_Level3_CC_EmpProcessList1);
//        EmpProcessArea G_Level3_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,G_Level3_CC_EmpProcessList2);
//
//        final List<EmpProcessArea> G_Level3_CC_Emp_Process_Areas = Arrays.asList(G_Level3_CC_Emp_Process_Area1,G_Level3_CC_Emp_Process_Area2);
//
//        employeeProcessAreaRepo.saveAll(G_Level3_CC_Emp_Process_Areas);
//
//        EmpProcessArea G_Level4_CC_Emp_Process_Area1 = new EmpProcessArea(ContractProcessArea1,G_Level4_CC_EmpProcessList1);
//        EmpProcessArea G_Level4_CC_Emp_Process_Area2 = new EmpProcessArea(ContractProcessArea2,G_Level4_CC_EmpProcessList2);
//
//        final List<EmpProcessArea> G_Level4_CC_Emp_Process_Areas = Arrays.asList(G_Level4_CC_Emp_Process_Area1,G_Level4_CC_Emp_Process_Area2);
//
//        employeeProcessAreaRepo.saveAll(G_Level4_CC_Emp_Process_Areas);
//
//
//        AdditionalDocument G_Level2_CC_Additional_document1 = new AdditionalDocument("GLevel2'sFILE1","#","descritpion");
//        additionalDocumentRepo.save(G_Level2_CC_Additional_document1);
//
//        AdditionalDocument G_Level3_CC_Additional_document1 = new AdditionalDocument("GLevel3FILE1","#","descritpion");
//        additionalDocumentRepo.save(G_Level3_CC_Additional_document1);
//
//        AdditionalDocument G_Level4_CC_Additional_document1 = new AdditionalDocument("GLevel4FILE1","#","descritpion");
//        additionalDocumentRepo.save(G_Level4_CC_Additional_document1);
//
//
//
//        EmpCertificateTaskDetails G_Level2_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
//        G_Level2_SavedDETAILS.setEmpProcessAreas(G_Level2_CC_Emp_Process_Areas);
//        G_Level2_SavedDETAILS.setRemarks(null);
//        G_Level2_SavedDETAILS.setAdditionalDocuments(Arrays.asList(G_Level2_CC_Additional_document1));
//        G_Level2_SavedDETAILS.setCertificate(null);
//
//        EmpCertificateTaskDetails G_Level2_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
//        G_Level2_SubmittedDetails.setEmpProcessAreas(null);
//        G_Level2_SubmittedDetails.setRemarks(null);
//        G_Level2_SubmittedDetails.setAdditionalDocuments(null);
//        G_Level2_SubmittedDetails.setCertificate(null);
//        G_Level2_SubmittedDetails.setCertificationStatus("Pending");
//
//        employeeCertificationTaskDetailsRepo.save(G_Level2_SavedDETAILS);
//        employeeCertificationTaskDetailsRepo.save(G_Level2_SubmittedDetails);
//
//
//        EmpCertificateTaskDetails G_Level3_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
//        G_Level3_SavedDETAILS.setEmpProcessAreas(G_Level3_CC_Emp_Process_Areas);
//        G_Level3_SavedDETAILS.setRemarks(null);
//        G_Level3_SavedDETAILS.setAdditionalDocuments(Arrays.asList(G_Level3_CC_Additional_document1));
//        G_Level3_SavedDETAILS.setCertificate(null);
//
//        EmpCertificateTaskDetails G_Level3_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
//        G_Level3_SubmittedDetails.setEmpProcessAreas(null);
//        G_Level3_SubmittedDetails.setRemarks(null);
//        G_Level3_SubmittedDetails.setAdditionalDocuments(null);
//        G_Level3_SubmittedDetails.setCertificate(null);
//        G_Level3_SubmittedDetails.setCertificationStatus("Pending");
//
//        employeeCertificationTaskDetailsRepo.save(G_Level3_SavedDETAILS);
//        employeeCertificationTaskDetailsRepo.save(G_Level3_SubmittedDetails);
//
//        EmpCertificateTaskDetails G_Level4_SavedDETAILS = new EmpCertificateTaskDetails("No","Pending",null);
//        G_Level4_SavedDETAILS.setEmpProcessAreas(G_Level4_CC_Emp_Process_Areas);
//        G_Level4_SavedDETAILS.setRemarks(null);
//        G_Level4_SavedDETAILS.setAdditionalDocuments(Arrays.asList(G_Level4_CC_Additional_document1));
//        G_Level4_SavedDETAILS.setCertificate(null);
//
//        EmpCertificateTaskDetails G_Level4_SubmittedDetails = new EmpCertificateTaskDetails("No","Pending",null);
//        G_Level4_SubmittedDetails.setEmpProcessAreas(null);
//        G_Level4_SubmittedDetails.setRemarks(null);
//        G_Level4_SubmittedDetails.setAdditionalDocuments(null);
//        G_Level4_SubmittedDetails.setCertificate(null);
//        G_Level4_SubmittedDetails.setCertificationStatus("Pending");
//
//        employeeCertificationTaskDetailsRepo.save(G_Level4_SavedDETAILS);
//        employeeCertificationTaskDetailsRepo.save(G_Level4_SubmittedDetails);
//
//
//        G_Level2_CC_Additional_document1.setEmpCertificationTaskDetailsId(G_Level2_SavedDETAILS.getId());
//        G_Level3_CC_Additional_document1.setEmpCertificationTaskDetailsId(G_Level3_SavedDETAILS.getId());
//        G_Level4_CC_Additional_document1.setEmpCertificationTaskDetailsId(G_Level3_SavedDETAILS.getId());
//
//        additionalDocumentRepo.saveAll(Arrays.asList(G_Level2_CC_Additional_document1,G_Level3_CC_Additional_document1,G_Level4_CC_Additional_document1));
//
//
//        EmpCertTask G_Level2_Task = new EmpCertTask(LocalDate.of(2020,6,1),G.getEmpId(),"Pending", G_Level2_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);
//
//        G_Level2_Task.setCertTask(contractualCompliance);
//        G_Level2_Task.setEmpBusinessUnit(G_Level2_Emp_BusinessUnit);
//        G_Level2_Task.setCertifierName(G.getName());
//        G_Level2_Task.setEmployeeId(G.getEmpId());
//        G_Level2_Task.setSavedDetails(G_Level2_SavedDETAILS);
//        G_Level2_Task.setSubmittedDetails(G_Level2_SubmittedDetails);
//        employeeCertificationTaskRepo.save(G_Level2_Task);
//
//
//        setCertificationTaskIdInEmpProcessArea(G_Level2_CC_Emp_Process_Areas,G_Level2_Task.getId());
//
//
//        EmpCertTask G_level3_Task = new EmpCertTask(LocalDate.of(2020,6,1),G.getEmpId(),"Pending", G_Level3_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);
//
//        G_level3_Task.setCertTask(contractualCompliance);
//        G_level3_Task.setEmpBusinessUnit(G_Level3_Emp_BusinessUnit);
//        G_level3_Task.setCertifierName(G.getName());
//        G_level3_Task.setEmployeeId(G.getEmpId());
//        G_level3_Task.setSavedDetails(G_Level3_SavedDETAILS);
//        G_level3_Task.setSubmittedDetails(G_Level3_SubmittedDetails);
//        employeeCertificationTaskRepo.save(G_level3_Task);
//
//
//        setCertificationTaskIdInEmpProcessArea(G_Level3_CC_Emp_Process_Areas,G_level3_Task.getId());
//
//
//        EmpCertTask G_level4_Task = new EmpCertTask(LocalDate.of(2020,6,1),G.getEmpId(),"Pending", G_Level4_SavedDETAILS,null/*X_IPAndE_SubmittedDetails*/);
//
//        G_level4_Task.setCertTask(contractualCompliance);
//        G_level4_Task.setEmpBusinessUnit(G_Level4_Emp_BusinessUnit);
//        G_level4_Task.setCertifierName(G.getName());
//        G_level4_Task.setEmployeeId(G.getEmpId());
//        G_level4_Task.setSavedDetails(G_Level4_SavedDETAILS);
//        G_level4_Task.setSubmittedDetails(G_Level4_SubmittedDetails);
//        employeeCertificationTaskRepo.save(G_level4_Task);
//
//
//        setCertificationTaskIdInEmpProcessArea(G_Level4_CC_Emp_Process_Areas,G_level4_Task.getId());
//
//        employeeProcessAreaRepo.saveAll(G_Level2_CC_Emp_Process_Areas);
//        employeeProcessAreaRepo.saveAll(G_Level3_CC_Emp_Process_Areas);
//        employeeProcessAreaRepo.saveAll(G_Level4_CC_Emp_Process_Areas);
//
//        G_Level2_SavedDETAILS.setEmpCertTaskId(G_Level2_Task.getId());
//        G_Level3_SavedDETAILS.setEmpCertTaskId(G_level3_Task.getId());
//        G_Level4_SavedDETAILS.setEmpCertTaskId(G_level4_Task.getId());
//
//        G_Level2_SubmittedDetails.setEmpCertTaskId(G_Level2_Task.getId());
//        G_Level3_SubmittedDetails.setEmpCertTaskId(G_level3_Task.getId());
//        G_Level4_SubmittedDetails.setEmpCertTaskId(G_level4_Task.getId());
//
//        employeeCertificationTaskDetailsRepo.saveAll(
//                Arrays.asList(G_Level2_SavedDETAILS,G_Level3_SavedDETAILS,G_Level4_SavedDETAILS,
//                        G_Level2_SubmittedDetails,G_Level3_SubmittedDetails,G_Level4_SubmittedDetails));
//
//        G.setCompany(TCS_Global);
//        G.setFunction(HR_Function);
//        G.setGeography(Geography_UK);
//
//        employeeRepo.saveAll(Arrays.asList(G));
//        employeeBusinessUnitRepo.saveAll(Arrays.asList(G_Level2_Emp_BusinessUnit,G_Level3_Emp_BusinessUnit,G_Level4_Emp_BusinessUnit));
//
//    }
//}
