package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.employeedetails.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepo extends JpaRepository<Company,Integer> {
}
