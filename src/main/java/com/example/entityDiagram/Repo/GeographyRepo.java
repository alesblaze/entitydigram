package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.employeedetails.Geography;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GeographyRepo extends JpaRepository<Geography,Integer> {
}
