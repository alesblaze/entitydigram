package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.employeedetails.Function;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FunctionRepo extends JpaRepository<Function,Integer> {
}
