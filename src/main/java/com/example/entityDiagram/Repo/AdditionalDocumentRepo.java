package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.certificationtaskdetails.AdditionalDocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdditionalDocumentRepo extends JpaRepository<AdditionalDocument,Integer> {
}
