package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.certificationtask.EmpCertTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeCertificationTaskRepo extends JpaRepository<EmpCertTask,Integer> {
    List<EmpCertTask> findByEmployeeId(String employeeId);
    List<EmpCertTask> findAllByCertTaskIdAndEmployeeId(Integer certificateId,String employeeId);
    EmpCertTask findByCertTaskIdAndEmployeeId(Integer certificateId,String employeeId);
    boolean existsBySavedDetailsIdAndEmployeeId(Integer savedDetailsId, String employeeId);
    List<EmpCertTask> findByEmployeeIdOrderByDueDateAsc(String employeeId);

    List<EmpCertTask> findByEmployeeIdOrderByCertifiedOnAsc(String employeeId);
    List<EmpCertTask> findByEmployeeIdOrderByCertifiedOnDesc(String employeeId);

    EmpCertTask findByIdAndEmployeeId(Integer empCertificateTask, String employeeId);
    List<EmpCertTask> findAllByEmpBusinessUnitParentBusinessUnitBusinessUnitName(String businessUnitName);
    List<EmpCertTask> findAllByEmpBusinessUnitParentBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpId(String businessUnitName, String employeeId);

    List<EmpCertTask> findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskName(String businessUnitName, String employeeId,String certificationTaskName);
    List<EmpCertTask> findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByDueDateAsc(String businessUnitName, String employeeId,String certificationTaskName);
    List<EmpCertTask> findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByDueDateDesc(String businessUnitName, String employeeId,String certificationTaskName);


    List<EmpCertTask> findByEmployeeIdOrderByDueDateDesc(String employeeId);

    List<EmpCertTask> findByEmployeeIdOrderByCertTaskStartDateAsc(String employeeId);
    List<EmpCertTask> findByEmployeeIdOrderByCertTaskStartDateDesc(String employeeId);

    List<EmpCertTask> findAllByEmpBusinessUnitParentBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskName(String businessUnitName, String employeeId, String certificationTaskName);

    List<EmpCertTask> findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByCertifiedOnDesc(String businessUnitName, String employeeId, String certificationTaskName);
    List<EmpCertTask> findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByCertifiedOnAsc(String businessUnitName, String employeeId, String certificationTaskName);

    List<EmpCertTask> findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByCertifierNameDesc(String businessUnitName, String employeeId, String certificationTaskName);
    List<EmpCertTask> findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByCertifierNameAsc(String businessUnitName, String employeeId, String certificationTaskName);

    List<EmpCertTask> findByEmployeeIdAndSavedDetailsCertificationStatus(String employeeId, String status);
}
