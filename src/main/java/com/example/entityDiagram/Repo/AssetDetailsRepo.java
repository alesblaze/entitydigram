package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.AssetDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssetDetailsRepo extends JpaRepository<AssetDetails,Integer> {
}
