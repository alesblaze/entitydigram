package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.certificationtaskdetails.EmpCertificateTaskDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeCertificationTaskDetailsRepo extends JpaRepository<EmpCertificateTaskDetails,Integer> {

//    @Modifying
//    @Query("UPDATE EmpCertificationTaskDetails ecd SET ecd.additionalDocument = null WHERE ecd.additionalDocument = :additionalDocumentId")
//    EmpCertificateTaskDetails updateAdditionalDetails(@Param("additionalDocumentId") Integer additionalDocumentId);



}
