package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.certificationtaskdetails.Certificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CertificateRepo extends JpaRepository<Certificate,Integer> {
    Certificate findByPath(String path);
}
