package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.businessunit.EmpBusinessUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeBusinessUnitRepo extends JpaRepository<EmpBusinessUnit,Integer> {
    List<EmpBusinessUnit> findAllByParentBusinessUnitBusinessUnitName(String businessUnitName);
}
