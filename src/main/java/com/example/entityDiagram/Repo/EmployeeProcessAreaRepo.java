package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.certificationtaskdetails.EmpProcessArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeProcessAreaRepo extends JpaRepository<EmpProcessArea,Integer> {
}
