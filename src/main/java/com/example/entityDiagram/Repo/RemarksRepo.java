package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.certificationtaskdetails.Remark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RemarksRepo extends JpaRepository<Remark,Integer> {
}
