package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.certificationtask.CertTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CertificationTaskRepo extends JpaRepository<CertTask,Integer> {
    boolean existsByName(String certificationTaskName);
}
