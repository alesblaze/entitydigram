package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.businessunit.BusinessUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BusinessUnitRepo extends JpaRepository<BusinessUnit,Integer> {
    boolean existsByName(String name);
}
