package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.certificationtaskdetails.EmpProcess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeProcessRepo extends JpaRepository<EmpProcess,Integer> {
}
