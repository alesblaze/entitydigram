package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee,Integer> {
    Employee findByEmpId(String empId);
    boolean existsByEmpId(String empId);
    Employee findByEmpIdAndPassword(String employeeId,String Password);
    boolean existsByEmpIdAndPassword(String employeeId, String password);

    @Query(value="SELECT name FROM employee WHERE emp_id=?1",nativeQuery=true)
    String getUserName(String emp_id);

    @Query(value="SELECT role FROM EMP_BUSINESS_UNIT WHERE emp_id=?1",nativeQuery=true)
    String getRole(String emp_id);
}
