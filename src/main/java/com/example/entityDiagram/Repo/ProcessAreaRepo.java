package com.example.entityDiagram.Repo;

import com.example.entityDiagram.entity.processarea.ProcessArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessAreaRepo extends JpaRepository<ProcessArea,Integer> {
}
