package com.example.entityDiagram.controller;

import com.example.entityDiagram.Repo.EmployeeRepo;
import com.example.entityDiagram.entity.Employee;
import com.example.entityDiagram.pojo.LoginRequest;
import com.example.entityDiagram.pojo.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//localhost:8080/employee/login


@RestController
@RequestMapping("employee")
@CrossOrigin
public class EmployeeLoginController {

    @Autowired
    private EmployeeRepo employeeRepo;

    @PostMapping("login")
    public @ResponseBody UserDetails loginEmployee(@RequestBody LoginRequest loginRequest) {
        boolean exists=employeeRepo.existsByEmpIdAndPassword(loginRequest.getEmployeeId(),loginRequest.getPassword());

        final Employee employee = employeeRepo.findByEmpId(loginRequest.getEmployeeId());

        UserDetails userdetails=new UserDetails();
        userdetails.setExists(exists);
//        employeeRepo.getUserName(loginRequest.getEmployeeId())
        userdetails.setUsername(employee.getName());
        String role = employee.getEmpBusinessUnitList().get(0).getRole();
        userdetails.setRole(role);
        return userdetails;
    }
}
