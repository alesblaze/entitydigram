package com.example.entityDiagram.controller;

import com.example.entityDiagram.Repo.CertificateRepo;
import com.example.entityDiagram.Repo.CertificationTaskRepo;
import com.example.entityDiagram.entity.certificationtaskdetails.Certificate;
import com.example.entityDiagram.pojo.CertificationTaskItem;
import com.example.entityDiagram.entity.certificationtask.CertTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//http://localhost:8080/coo/view/certificate

@RestController
@RequestMapping("coo")
@CrossOrigin
public class COOCertificationTaskController {

    @Autowired
    private CertificationTaskRepo certificationTaskRepo;

    @GetMapping("tasks")
    public List<CertificationTaskItem> getAllCertificationTasksForCOO() {
        final List<CertTask> certTasks = certificationTaskRepo.findAll();

        List<CertificationTaskItem> taskItemList = new ArrayList<>();

        for (CertTask certTask: certTasks) {
            final Integer certificateTaskId = certTask.getId();
            final String name = certTask.getName();
            final LocalDate startDate = certTask.getStartDate();
            final LocalDate endDate = certTask.getEndDate();
            CertificationTaskItem certificationTaskItem = new CertificationTaskItem(certificateTaskId,name,startDate,endDate);
            taskItemList.add(certificationTaskItem);
        }
        return taskItemList;
    }

    @Autowired
    private CertificateRepo certificateRepo;

    @GetMapping("/view/certificate/{id}")
    public ResponseEntity<ByteArrayResource> getCertificate(@PathVariable Integer id) {
        final Certificate certificate = certificateRepo.findById(id).get();
        final Path path = Paths.get(certificate.getPath());

        try {
            final byte[] filesBytes = Files.readAllBytes(path);
            ByteArrayResource resource = new ByteArrayResource(filesBytes);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .contentLength(resource.contentLength())
                    .body(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
    }
}
