package com.example.entityDiagram.controller;

import com.example.entityDiagram.Repo.AdditionalDocumentRepo;
import com.example.entityDiagram.Repo.EmployeeCertificationTaskDetailsRepo;
import com.example.entityDiagram.Repo.EmployeeCertificationTaskRepo;
import com.example.entityDiagram.pojo.AdditionalDocumentRequest;
import com.example.entityDiagram.entity.certificationtaskdetails.AdditionalDocument;
import com.example.entityDiagram.entity.certificationtaskdetails.EmpCertificateTaskDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static com.example.entityDiagram.controller.CertifierCertifyingController.CertificationTaskNotFound;

//http://localhost:8080/additional_document

@RestController
@RequestMapping("additional_document")
@CrossOrigin
public class AdditionalDocumentController {
    private static String DIR_TO_UPLOAD = System.getProperty("user.home") + File.separator + "documents";;


    static {
        Path directory = Paths.get(DIR_TO_UPLOAD);
        if(!Files.exists(directory)) {
            try {
                Files.createDirectory(Paths.get(DIR_TO_UPLOAD));
                DIR_TO_UPLOAD += File.separator;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Autowired
    private AdditionalDocumentRepo additionalDocumentRepo;
    @Autowired
    private EmployeeCertificationTaskDetailsRepo employeeCertificationTaskDetailsRepo;
    @Autowired
    private EmployeeCertificationTaskRepo employeeCertificationTaskRepo;

    static class FileValidationErrorResponse {
        private String fileValidationError;

        public FileValidationErrorResponse(String fileValidationError) {
            this.fileValidationError = fileValidationError;
        }

        public String getFileValidationError() {
            return fileValidationError;
        }

        public void setFileValidationError(String fileValidationError) {
            this.fileValidationError = fileValidationError;
        }
    }

    static class SubmitDocumentsResponse {
        private Integer empCertificationTaskDetailsId;
        private boolean submit;

        public Integer getEmpCertificationTaskDetailsId() {
            return empCertificationTaskDetailsId;
        }

        public void setEmpCertificationTaskDetailsId(Integer empCertificationTaskDetailsId) {
            this.empCertificationTaskDetailsId = empCertificationTaskDetailsId;
        }

        public boolean isSubmit() {
            return submit;
        }

        public void setSubmit(boolean submit) {
            this.submit = submit;
        }
    }

    @PostMapping("/submitted")
    public ResponseEntity<?> submitDocuments(@RequestBody() SubmitDocumentsResponse submitResponse){
        final Integer empCertificationTaskDetailsId = submitResponse.getEmpCertificationTaskDetailsId();
        final boolean submit = submitResponse.isSubmit();

        EmpCertificateTaskDetails empCertificateTaskDetails = employeeCertificationTaskDetailsRepo
                .findById(empCertificationTaskDetailsId).orElse(null);
        if (empCertificateTaskDetails == null) return new ResponseEntity<>(new CertificationTaskDetailsNotFoundResponse(empCertificationTaskDetailsId) ,HttpStatus.BAD_REQUEST);
        empCertificateTaskDetails.setAdditionalDocumentsSubmitted(submit);
        employeeCertificationTaskDetailsRepo.save(empCertificateTaskDetails);
        return new ResponseEntity<>(empCertificateTaskDetails.isAdditionalDocumentsSubmitted(),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> uploadDocument(@RequestParam MultipartFile file,
                                            @RequestParam String description,
                                            @RequestParam Integer employeeCertificationTaskSavedDetailId,
                                            @RequestParam String employeeId) throws IOException {
        //TODO : maximum file size
        final String fileName = file.getOriginalFilename();
        String validate = validateFile(file, fileName);
        if (!validate.equals("")) return new ResponseEntity<>(new FileValidationErrorResponse(validate), HttpStatus.BAD_REQUEST);

        final AdditionalDocument savedDocument = saveAdditionalDocsToDb(description, employeeCertificationTaskSavedDetailId, fileName, employeeId);
        if (savedDocument==null) return new ResponseEntity(new FileValidationErrorResponse("File Upload Issue") ,HttpStatus.BAD_REQUEST);

        byte[] bytes = file.getBytes();

        Path path = Paths.get(DIR_TO_UPLOAD + File.separator + fileName);
        Files.write(path, bytes);

//        TODO : set the additional document tab to block

        return new ResponseEntity<>(savedDocument,HttpStatus.OK);
    }

    static class FileNotExistsResponse {
        private String fileNotExists;

        public FileNotExistsResponse(Integer documentId) {
            this.fileNotExists = String.format("Document with this Document Id = '%d' doesn't exists",documentId);
        }

        public String getFileNotExists() {
            return fileNotExists;
        }

        public void setFileNotExists(String fileNotExists) {
            this.fileNotExists = fileNotExists;
        }
    }

    static class CertificationTaskDetailsNotFoundResponse {
        private String taskDetailsNotFound;

        public CertificationTaskDetailsNotFoundResponse(Integer certificationTaskDetailsId) {
            this.taskDetailsNotFound = String.format("Certification Task Details with this Id='%d' not found.",certificationTaskDetailsId);
        }

        public String getTaskDetailsNotFound() {
            return taskDetailsNotFound;
        }

        public void setTaskDetailsNotFound(String taskDetailsNotFound) {
            this.taskDetailsNotFound = taskDetailsNotFound;
        }
    }


    //TODO : Yet to Implement in front end
    @PostMapping("/delete")
//    ResponseEntity<List<AdditionalDocument>>
    public ResponseEntity<?> deleteAdditionalDetails
            (@RequestBody AdditionalDocumentRequest additionalDocumentRequest) {
        final Integer additionalDetailsId = additionalDocumentRequest.getAdditionalDetailsId();
        final Integer empCertificationTaskDetailsId = additionalDocumentRequest.getEmpCertificationTaskDetailsId();
        final String employeeId = additionalDocumentRequest.getEmployeeId();

        //Whether object requested to be deleted present of not
        final boolean doesAdditionalDocExists = additionalDocumentRepo.existsById(additionalDetailsId);
        if (!doesAdditionalDocExists) return new ResponseEntity<>(new FileNotExistsResponse(additionalDetailsId),HttpStatus.BAD_REQUEST);

        //Fetch EmpCert task to update additional details association in it
        EmpCertificateTaskDetails empCertificateTaskDetails = employeeCertificationTaskDetailsRepo
                .findById(empCertificationTaskDetailsId).orElse(null);
        if (empCertificateTaskDetails == null) return new ResponseEntity<>(new CertificationTaskDetailsNotFoundResponse(empCertificationTaskDetailsId) ,HttpStatus.BAD_REQUEST);

        //Whether the employeeCertification task is associated to requester employee's Id
        final boolean employeeCertificationTask = employeeCertificationTaskRepo
                .existsBySavedDetailsIdAndEmployeeId(empCertificationTaskDetailsId, employeeId);

        final String notAssociatedResponse = String.format("Certification Task Not Found With Certification Task Id='%d' and Employee id = '%s'", empCertificationTaskDetailsId, employeeId);
        if (!employeeCertificationTask) return new ResponseEntity<>(new CertificationTaskNotFound(notAssociatedResponse),HttpStatus.BAD_REQUEST);

        final AdditionalDocument additionalDocument = additionalDocumentRepo.findById(additionalDetailsId).get();
        empCertificateTaskDetails.getAdditionalDocuments().remove(additionalDocument);
        empCertificateTaskDetails = employeeCertificationTaskDetailsRepo.save(empCertificateTaskDetails);
        return new ResponseEntity(empCertificateTaskDetails.getAdditionalDocuments(),HttpStatus.ACCEPTED);
    }

    private AdditionalDocument  saveAdditionalDocsToDb(String description, Integer savedCertificationTaskDetails, String fileName, String employeeId) {
        EmpCertificateTaskDetails empCertificateTaskDetails = employeeCertificationTaskDetailsRepo.findById(savedCertificationTaskDetails).get();

        if (empCertificateTaskDetails == null) return null;

        final boolean isItAssociated = employeeCertificationTaskRepo.existsBySavedDetailsIdAndEmployeeId(savedCertificationTaskDetails, employeeId);
        if (!isItAssociated) return null;
//        Bad Data Sent

        final int size = empCertificateTaskDetails.getAdditionalDocuments().size();
        if (size == 5) {
//            TODO : show file limit surpassed
            return null;
        }

        AdditionalDocument additionalDocument = new AdditionalDocument(fileName,DIR_TO_UPLOAD + fileName, description);
        additionalDocument.setEmpCertificationTaskDetailsId(savedCertificationTaskDetails);
        final AdditionalDocument savedDocument = additionalDocumentRepo.save(additionalDocument);
        empCertificateTaskDetails.getAdditionalDocuments().add(additionalDocument);
        employeeCertificationTaskDetailsRepo.save(empCertificateTaskDetails);
        return savedDocument;
    }

    private String validateFile(MultipartFile file, String fileName) {
        final long fileSizeKB = file.getSize() / 1024;

        if (fileSizeKB > 20480) {
            return "File Is Greater than 20MB";
        }

        final boolean isFileValid = verifyFileFormat(fileName.substring(fileName.indexOf(".") + 1));
        final boolean isNameWithoutDash = verifyDashes(fileName);
        if (!isFileValid) {
            return "File Format is Not Acceptable!";
        }
        if (!isNameWithoutDash) {
            return "File Name Contains either '-'or '_'or '.'";
        }
        return "";
    }

    private boolean verifyFileFormat(String fileFormat) {
        if (emptyStringCheck(fileFormat)) return false;
        List<String> validTypes = Arrays.asList("pdf","doc","docx","txt","xls","xlsx","jpeg","png");

        if (validTypes.contains(fileFormat) ) {
            return true;
        } else {
            return false;
        }
    }

    private boolean verifyDashes(String fileName) {
        if (emptyStringCheck(fileName)) return true;
        final int lastIndex = fileName.lastIndexOf(".");
        final String substring = fileName.substring(0, lastIndex);
        if (fileName.contains("-") || fileName.contains("_") || substring.contains(".")) {
            return false;
        } else {
            return true;
        }
    }

    private boolean emptyStringCheck(String fileName) {
        if (fileName == null || fileName.trim().equals("")) {
            return true;
        }
        return false;
    }



}
