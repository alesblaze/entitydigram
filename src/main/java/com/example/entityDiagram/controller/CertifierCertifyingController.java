package com.example.entityDiagram.controller;

import com.example.entityDiagram.Repo.*;
import com.example.entityDiagram.entity.*;
import com.example.entityDiagram.entity.businessunit.EmpBusinessUnit;
import com.example.entityDiagram.entity.certificationtask.EmpCertTask;
import com.example.entityDiagram.entity.certificationtaskdetails.Remark;
import com.example.entityDiagram.entity.certificationtaskdetails.EmpProcessArea;
import com.example.entityDiagram.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

//localhost:8080/certifying/certificationTask/certificateId

@RestController
@RequestMapping("certifying")
@CrossOrigin
public class CertifierCertifyingController {

    @Autowired
    private EmployeeCertificationTaskRepo employeeCertificationTaskRepo;

    @Autowired
    private EmployeeProcessAreaRepo employeeProcessAreaRepo;

    @Autowired
    private EmployeeProcessRepo employeeProcessRepo;

    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private RemarksRepo remarksRepo;

    static class CertificationTaskNotFound {
        String taskNotFound;

        public CertificationTaskNotFound(Integer certificationTaskId) {
            this.taskNotFound = String.format("Certification Task with certification Task Id='%d' is not Found ",certificationTaskId);
        }

        public CertificationTaskNotFound(String taskNotFound) {
            this.taskNotFound = taskNotFound;
        }

        public String getTaskNotFound() {
            return taskNotFound;
        }

        public void setTaskNotFound(String taskNotFound) {
            this.taskNotFound = taskNotFound;
        }

        @Override
        public String toString() {
            return "CertificationTaskNotFound{" +
                    "taskNotFound='" + taskNotFound + '\'' +
                    '}';
        }
    }

    static class FunctionalityNotAllowedResponse {
        private String notAllowed;

        public FunctionalityNotAllowedResponse() {
            this.notAllowed = "This Functionality is Not Allowed For the One Requesting.";
        }

        public String getNotAllowed() {
            return notAllowed;
        }

        public void setNotAllowed(String notAllowed) {
            this.notAllowed = notAllowed;
        }
    }

    //Input : emp_certification_task_id & employeeId
    @PostMapping("certificationTask/certificateId")
//    ResponseEntity<CertificateTaskResponse>
    public ResponseEntity<?> getEmployeeCertificationTask(@RequestBody CertificationTaskRequest certificationTaskRequest) {
        final Integer certificateId = certificationTaskRequest.getCertificateId();
        final String employeeId = certificationTaskRequest.getEmployeeId();

        final EmpCertTask employeeCertTask = employeeCertificationTaskRepo.findByIdAndEmployeeId(certificateId, employeeId);

        if (employeeCertTask == null) return new ResponseEntity<>( new CertificationTaskNotFound(certificateId), HttpStatus.BAD_REQUEST);

        final EmpBusinessUnit parentBusinessUnit = employeeCertTask.getEmpBusinessUnit().getParentBusinessUnit();

        if (parentBusinessUnit == null) {
            return new ResponseEntity<>(new FunctionalityNotAllowedResponse(),HttpStatus.BAD_REQUEST);
        }

        final EmpBusinessUnit empBusinessUnit = employeeCertTask.getEmpBusinessUnit();
        final CertifierResponse certifierResponse = getCertifierResponse(employeeId, employeeCertTask, empBusinessUnit);
        final ReporterResponse reporterResponse = getReporterResponse(parentBusinessUnit, empBusinessUnit);
        final CertificateTaskResponse certificateTaskResponse = new CertificateTaskResponse(certifierResponse,reporterResponse,employeeCertTask);

        return new ResponseEntity<CertificateTaskResponse>(certificateTaskResponse,HttpStatus.OK);
    }


    @PostMapping("save/processes")
//    ResponseEntity<List<EmpProcessArea>>
    public ResponseEntity<?> saveEmpProcessArea(@RequestBody EmpProcessAreaRequest empProcessAreaRequest) {
        final List<EmpProcessArea> empProcessAreas = empProcessAreaRequest.getEmpProcessAreas();
        final Integer empCertificationTaskId = empProcessAreaRequest.getEmpCertificationTaskId();
        final String employeeId = empProcessAreaRequest.getEmployeeId();

        final EmpCertTask empCertTask =
                employeeCertificationTaskRepo.findByIdAndEmployeeId(empCertificationTaskId, employeeId);

        if (empCertTask==null) return new ResponseEntity<>(new CertificationTaskNotFound(empCertificationTaskId),HttpStatus.BAD_REQUEST);

        List<EmpProcessArea> empProcessList = new ArrayList<>();
        for (EmpProcessArea empProcessArea: empProcessAreas) {
            empProcessList.add(employeeProcessAreaRepo.save(empProcessArea));
        }
        empCertTask.setProcessSubmitted(true);
        employeeCertificationTaskRepo.save(empCertTask);

        return new ResponseEntity<>(empProcessList,HttpStatus.OK);
    }

    @Modifying
    @PostMapping("save/remarks")
//    List<Remark>
    public ResponseEntity<?> saveEmpRemarks(@RequestBody RemarksRequest remarksRequest) {
        final Integer empCertificationTaskId = remarksRequest.getEmpCertificationTaskId();
        final String employeeId = remarksRequest.getEmployeeId();

        final EmpCertTask empCertTask = employeeCertificationTaskRepo.findByIdAndEmployeeId(empCertificationTaskId, employeeId);
        if (empCertTask==null) return new ResponseEntity<>(new CertificationTaskNotFound(empCertificationTaskId),HttpStatus.BAD_REQUEST);

        final List<Remark> remarks = remarksRequest.getRemarks();
        List<Remark> remarkList = remarksRepo.saveAll(remarks);

        empCertTask.setRemarksSubmitted(true);
	employeeCertificationTaskRepo.save(empCertTask);

        return new ResponseEntity<>(remarkList,HttpStatus.OK);
    }


    private ReporterResponse getReporterResponse(EmpBusinessUnit parentBusinessUnit, EmpBusinessUnit empBusinessUnit) {
        final String reportingToEmpId = empBusinessUnit.getReportingToEmpId();

        final Employee reporter = employeeRepo.findByEmpId(reportingToEmpId);
        final String nameOfReporter = reporter.getName();
        final String reporterRole = parentBusinessUnit.getRole();
        final String parentBusinessUnitName = parentBusinessUnit.getBusinessUnit().getName();

        ReporterResponse reporterResponse = new ReporterResponse(nameOfReporter,reporterRole,parentBusinessUnitName);
        return reporterResponse;
    }

    private CertifierResponse getCertifierResponse(String employeeId, EmpCertTask employeeCertTask, EmpBusinessUnit empBusinessUnit) {
        final Employee employee = employeeRepo.findByEmpId(employeeId);

        final String employeeName = employee.getName();
        final String role = employeeCertTask.getEmpBusinessUnit().getRole();
        final String businessUnitName = empBusinessUnit.getBusinessUnit().getName();
        final String employeeFunction = employee.getFunction().getName();
        final String employeeGeography = employee.getGeography().getName();
        final String employeeCompany = employee.getCompany().getName();

        CertifierResponse certifierResponse = new CertifierResponse(employeeName,role,businessUnitName,employeeCompany,employeeGeography,employeeFunction);
        return certifierResponse;
    }


}
