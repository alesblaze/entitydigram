package com.example.entityDiagram.controller;

import com.example.entityDiagram.Repo.CertificationTaskRepo;
import com.example.entityDiagram.Repo.EmployeeCertificationTaskRepo;
import com.example.entityDiagram.pojo.CertificationItem;
import com.example.entityDiagram.pojo.ItemTask;
import com.example.entityDiagram.entity.certificationtask.CertTask;
import com.example.entityDiagram.entity.certificationtask.EmpCertTask;
import com.example.entityDiagram.entity.certificationtaskdetails.EmpCertificateTaskDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
    //localhost:8080/certification_task/all

//localhost:8080/certification_task/tasks/filter/certificate_name

//localhost:8080/certification_task/tasks?employeeId=111000

//localhost:8080/certification_task/tasks/filter/dueDate/asc

//localhost:8080/certification_task/tasks/filter/certifiedOn/asc

@RestController
@RequestMapping("certification_task")
@CrossOrigin
public class EmployeeCertificationTaskController {

    @Autowired
    private CertificationTaskRepo certificationTaskRepo;

    @Autowired
    private EmployeeCertificationTaskRepo employeeCertificationTaskRepo;

    //TODO: get all the List of Certification Task name
    @GetMapping("all")
    public List<CertificationItem> getAllCertificationTasks() {
        final List<CertTask> allCertificationTask = certificationTaskRepo.findAll();

        List<CertificationItem> certificationItems = new ArrayList<>();

        for (CertTask certificationTask: allCertificationTask) {
            CertificationItem certificationTaskItem = new CertificationItem(certificationTask.getId(),certificationTask.getName());
            certificationItems.add(certificationTaskItem);
        }
        return certificationItems;
    }

    //TODO : get all the List Of Employee's Certification Task name
    @PostMapping("tasks")
    public List<ItemTask> getEmployeeAllCertificationTask(@RequestParam("employeeId") String employeeId) {
        //if certifier is fetching then fetch old details
        List<ItemTask> itemTaskList = new ArrayList<>();

        final List<EmpCertTask> empCertTaskList =
                employeeCertificationTaskRepo.findByEmployeeIdAndSavedDetailsCertificationStatus(employeeId,"Pending");

        fromEmpCertTaskToItemTask(empCertTaskList, itemTaskList);
        return itemTaskList;
    }

    static class CertificationTaskRequest {
        private String employeeId;
        private Integer certificationTaskId;

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public Integer getCertificationTaskId() {
            return certificationTaskId;
        }

        public void setCertificationTaskId(Integer certificationTaskId) {
            this.certificationTaskId = certificationTaskId;
        }
    }

    //TODO : get all cert. tasks acc. to cert. name and for an employee
    @PostMapping("tasks/filter/certificate_name")
    public List<ItemTask> getAllCertificationTasksWithName(
            @RequestBody CertificationTaskRequest request ) {

        //find cert task by id
        Integer certificationId = request.getCertificationTaskId();
        String empId = request.getEmployeeId();

        List<ItemTask> itemTaskList = new ArrayList<>();

        final List<EmpCertTask> empCertTaskList = employeeCertificationTaskRepo.findAllByCertTaskIdAndEmployeeId(certificationId,empId);

        fromEmpCertTaskToItemTask(empCertTaskList, itemTaskList);
        return itemTaskList;
    }


    @GetMapping("tasks/filter/dueDate/asc")
    public List<ItemTask> getAllEmployeeTaskFilteredByDueDateAsc(@RequestParam("employeeId") String employeeId) {
        final List<EmpCertTask> empCertTaskList = employeeCertificationTaskRepo.findByEmployeeIdOrderByDueDateAsc(employeeId);
        List<ItemTask> itemTaskList = new ArrayList<>();

        fromEmpCertTaskToItemTask(empCertTaskList, itemTaskList);
        return itemTaskList;
    }

    @GetMapping("tasks/filter/dueDate/desc")
    public List<ItemTask> getAllEmployeeTaskFilteredByDueDateDesc(@RequestParam("employeeId") String employeeId) {
        final List<EmpCertTask> empCertTaskList = employeeCertificationTaskRepo.findByEmployeeIdOrderByDueDateDesc(employeeId);
        List<ItemTask> itemTaskList = new ArrayList<>();
        fromEmpCertTaskToItemTask(empCertTaskList, itemTaskList);
        return itemTaskList;
    }

    private void fromEmpCertTaskToItemTask(List<EmpCertTask> empCertTaskList, List<ItemTask> itemTaskList) {
        for (EmpCertTask empCertTask : empCertTaskList) {
            final CertTask certTask = empCertTask.getCertTask();
            final EmpCertificateTaskDetails submittedDetails = empCertTask.getSubmittedDetails();

            ItemTask itemTask = new ItemTask(empCertTask.getId(),certTask.getName(),certTask.getStartDate(),
                    certTask.getEndDate(),certTask.getPlannedStartDate(),
                    empCertTask.getDueDate(),empCertTask.getCertifiedOn(),submittedDetails==null?null:submittedDetails.getCertificationStatus());

            itemTaskList.add(itemTask);
        }
    }

    // Filters emp certification task by certifiedOn date
    @GetMapping("tasks/filter/certifiedOn/asc")
    public List<ItemTask> getAllEmployeeTaskFilteredByCertifiedOnDateAsc(@RequestParam("employeeId") String employeeId) {
        final List<EmpCertTask> empCertTaskList = employeeCertificationTaskRepo.findByEmployeeIdOrderByCertifiedOnAsc(employeeId);
        List<ItemTask> itemTaskList = new ArrayList<>();

        fromEmpCertTaskToItemTask(empCertTaskList, itemTaskList);
        return itemTaskList;
    }

    @GetMapping("tasks/filter/certifiedOn/desc")
    public List<ItemTask> getAllEmployeeTaskFilteredByCertifiedOnDateDesc(@RequestParam("employeeId") String employeeId) {
        final List<EmpCertTask> empCertTaskList = employeeCertificationTaskRepo.findByEmployeeIdOrderByCertifiedOnAsc(employeeId);
        List<ItemTask> itemTaskList = new ArrayList<>();

        fromEmpCertTaskToItemTask(empCertTaskList, itemTaskList);
        return itemTaskList;
    }

    @GetMapping("tasks/filter/plannedStart/asc")
    public List<ItemTask> getAllEmployeeTaskFilteredByPlannedStartAsc(@RequestParam("employeeId") String employeeId) {
        final List<EmpCertTask> empCertTaskList = employeeCertificationTaskRepo.findByEmployeeIdOrderByCertTaskStartDateAsc(employeeId);
        List<ItemTask> itemTaskList = new ArrayList<>();
        fromEmpCertTaskToItemTask(empCertTaskList, itemTaskList);
        return itemTaskList;
    }

    @GetMapping("tasks/filter/plannedStart/desc")
    public List<ItemTask> getAllEmployeeTaskFilteredByPlannedStartDesc(@RequestParam("employeeId") String employeeId) {
        final List<EmpCertTask> empCertTaskList = employeeCertificationTaskRepo.findByEmployeeIdOrderByCertTaskStartDateDesc(employeeId);
        List<ItemTask> itemTaskList = new ArrayList<>();
        fromEmpCertTaskToItemTask(empCertTaskList, itemTaskList);
        return itemTaskList;
    }

}
