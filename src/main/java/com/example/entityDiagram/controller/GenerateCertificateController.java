package com.example.entityDiagram.controller;

import com.example.entityDiagram.Repo.*;
import com.example.entityDiagram.controller.CertifierCertifyingController.CertificationTaskNotFound;
import com.example.entityDiagram.controller.CertifierCertifyingController.FunctionalityNotAllowedResponse;
import com.example.entityDiagram.entity.certificationtask.EmpCertTask;
import com.example.entityDiagram.entity.certificationtaskdetails.Certificate;
import com.example.entityDiagram.entity.certificationtaskdetails.EmpCertificateTaskDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

//localhost:8080/generate

@RestController
@RequestMapping("generate")
@CrossOrigin
public class GenerateCertificateController {

    private static String DIR_TO_UPLOAD = System.getProperty("user.home") + File.separator + "certificate";


    static {
        Path directory = Paths.get(DIR_TO_UPLOAD);
        if(!Files.exists(directory)) {
            try {
                Files.createDirectory(Paths.get(DIR_TO_UPLOAD));
                DIR_TO_UPLOAD += File.separator;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Autowired
    private EmployeeCertificationTaskRepo employeeCertificationTaskRepo;

    @Autowired
    private EmployeeCertificationTaskDetailsRepo employeeCertificationTaskDetailsRepo;

    @Autowired
    private CertificateRepo certificateRepo;
    @Autowired
    private AdditionalDocumentRepo additionalDocumentRepo;
    @Autowired
    private RemarksRepo remarksRepo;
    @Autowired
    private EmployeeProcessRepo empProcessRepo;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> generateCertificate(
            @RequestParam Integer certificateTaskId , @RequestParam String empId , @RequestParam MultipartFile certificate)
            throws IOException {

        final EmpCertTask employeeCertificationTask = employeeCertificationTaskRepo.findByIdAndEmployeeId(certificateTaskId, empId);

        if (employeeCertificationTask == null) {
            return new ResponseEntity<>(new CertificationTaskNotFound(certificateTaskId), HttpStatus.BAD_REQUEST);
        }

        final EmpCertificateTaskDetails savedDetails = employeeCertificationTask.getSavedDetails();
        final EmpCertificateTaskDetails submittedDetails = employeeCertificationTask.getSubmittedDetails();

        final String businessUnitName = employeeCertificationTask.getEmpBusinessUnit().getBusinessUnit().getName();
        final String certificationTaskName = employeeCertificationTask.getCertTask().getName();

        final List<EmpCertTask> taskDownBelow = employeeCertificationTaskRepo.findAllByEmpBusinessUnitParentBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskName(businessUnitName, empId, certificationTaskName);

        final Boolean[] isAllowed = {false};
        if (taskDownBelow == null || taskDownBelow.size() == 0) {
            isAllowed[0]=true;
        } else {
            taskDownBelow.forEach(empCertTask -> {
                if (empCertTask != null
                        && empCertTask.getSubmittedDetails() != null
                        && empCertTask.getSubmittedDetails().getCertificationStatus() != null
                        && empCertTask.getSubmittedDetails().getCertificationStatus().equals("Certified")) {
                    isAllowed[0] = true;
                } else {
                    isAllowed[0] = false;
                    return;
                }
            });
        }

        if (!isAllowed[0]) {
            return new ResponseEntity<>(new FunctionalityNotAllowedResponse(),HttpStatus.BAD_REQUEST);
        }

        //check if they have declared themselves as compliant
        final Boolean[] compliance = {true};
        savedDetails.getEmpProcessAreas().forEach(empProcessArea -> {
            empProcessArea.getEmpProcesses().forEach(empProcess -> {
                if(empProcess.getIsCompliant().equals("No")) {
                    compliance[0] = false;
                }
            });
        });

        //TODO : what kind of certification task this is
        if (employeeCertificationTask.getCertTask().getName().equals("IP & Engineering Certificate")) {
            savedDetails.getRemarks().forEach(remark -> {
                if (remark.getIsCompliant().equals("No")) {
                    compliance[0] = false;
                }
            });
        }

        if (compliance[0]) {
            savedDetails.setIsCompliant("Yes");
        } else {
            savedDetails.setIsCompliant("No");
        }

        savedDetails.setCertificationStatus("Certified");
        employeeCertificationTask.setCertifiedOn(LocalDate.now());

        //TODO: associate the generated certificate

        byte[] bytes = certificate.getBytes();
        Path path = Paths.get(DIR_TO_UPLOAD + File.separatorChar + certificate.getName());
        Files.write(path, bytes);
        //Certificate Upload Done.

        Certificate certificate1  = new Certificate(DIR_TO_UPLOAD + File.separatorChar + certificate.getName());
        certificate1.setEmpCertificationTaskDetailsId(employeeCertificationTask.getSubmittedDetails().getId());

        EmpCertificateTaskDetails deepCopy = new EmpCertificateTaskDetails();

        deepCopy.setId(submittedDetails.getId());
        deepCopy.setCertificate(certificate1);
        deepCopy.setCertificationStatus("Certified");
        deepCopy.setAdditionalDocuments(null);
        deepCopy.setRemarks(null);
        deepCopy.setEmpProcessAreas(null);
        deepCopy.setIsCompliant(savedDetails.getIsCompliant());
        deepCopy.setEmpCertTaskId(employeeCertificationTask.getId());

        certificateRepo.save(deepCopy.getCertificate());
        employeeCertificationTaskDetailsRepo.saveAll(Arrays.asList(savedDetails,deepCopy));

        employeeCertificationTask.setSubmittedDetails(deepCopy);

        employeeCertificationTaskRepo.saveAndFlush(employeeCertificationTask);

        return new ResponseEntity<>(certificate1,HttpStatus.OK);
    }
}
