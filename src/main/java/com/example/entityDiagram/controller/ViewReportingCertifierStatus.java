package com.example.entityDiagram.controller;

import com.example.entityDiagram.Repo.*;
import com.example.entityDiagram.pojo.CertifierReport;
import com.example.entityDiagram.pojo.ReportResponse;
import com.example.entityDiagram.pojo.UnitDetails;
import com.example.entityDiagram.entity.businessunit.EmpBusinessUnit;
import com.example.entityDiagram.entity.certificationtask.EmpCertTask;
import com.example.entityDiagram.entity.certificationtaskdetails.EmpCertificateTaskDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

//http://localhost:8080/view_report/view

@RestController
@RequestMapping("/view_report")
@CrossOrigin
public class ViewReportingCertifierStatus {

    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private EmployeeCertificationTaskRepo employeeCertificationTaskRepo;

    @Autowired
    private BusinessUnitRepo businessUnitRepo;

    @Autowired
    private CertificationTaskRepo certificationTaskRepo;

    @Autowired
    private EmployeeBusinessUnitRepo employeeBusinessUnitRepo;

    static class InvalidBusinessUnitResponse {
        private String invalidBusinessUnitName;

        public InvalidBusinessUnitResponse(String businessUnitName) {
            this.invalidBusinessUnitName = String.format("Business Unit with this name='%s' is invalid",businessUnitName);
        }

        public String getInvalidBusinessUnitName() {
            return invalidBusinessUnitName;
        }

        public void setInvalidBusinessUnitName(String invalidBusinessUnitName) {
            this.invalidBusinessUnitName = invalidBusinessUnitName;
        }
    }

    static class InvalidCertificationTaskResponse {
        private String certificationTaskNotFound;

        public InvalidCertificationTaskResponse(String certificationTaskName) {
            this.certificationTaskNotFound = String.format("Certification Task with this name='%s' doesn't exists.",certificationTaskName);
        }

        public String getCertificationTaskNotFound() {
            return certificationTaskNotFound;
        }

        public void setCertificationTaskNotFound(String certificationTaskNotFound) {
            this.certificationTaskNotFound = certificationTaskNotFound;
        }
    }

    static class InvalidEmployeeIdResponse {
        private String invalidEmployeeId;

        public InvalidEmployeeIdResponse(String invalidEmployeeId) {
            this.invalidEmployeeId = String.format("Employee with this Employee Id ='%s' Not Found",invalidEmployeeId);
        }

        public String getInvalidEmployeeId() {
            return invalidEmployeeId;
        }

        public void setInvalidEmployeeId(String invalidEmployeeId) {
            this.invalidEmployeeId = invalidEmployeeId;
        }
    }

    @PostMapping("/view")
    public ResponseEntity<?> viewReportingStatusOfAllBusinessUnit(
            @RequestBody CertifierReport certifierReport) {
        final String employeeId = certifierReport.getEmpId().trim();
        final String businessUnitName = certifierReport.getBusinessUnitName().trim();
        final String certificationTaskName = certifierReport.getCertificationTaskName().trim();

        ResponseEntity<?> validationResponse = validateForInputForViewReportingCertifierStatus(employeeId, businessUnitName, certificationTaskName);
        if (validationResponse != null) return validationResponse;
        //looking from below to up in hierarchy
        final List<EmpCertTask> allEmpDownOneLevel = employeeCertificationTaskRepo.findAllByEmpBusinessUnitParentBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskName(businessUnitName,employeeId,certificationTaskName);
        List<ReportResponse> reportResponses = empCertTaskToReportResponse(allEmpDownOneLevel);

        return new ResponseEntity<>(reportResponses,HttpStatus.OK);
    }

    private ResponseEntity<?> validateForInputForViewReportingCertifierStatus(String employeeId, String businessUnitName, String certificationTaskName) {
        final boolean doesEmployeeExists = employeeRepo.existsByEmpId(employeeId);
        if (!doesEmployeeExists) {
            return new ResponseEntity<>(new InvalidEmployeeIdResponse(employeeId),HttpStatus.BAD_REQUEST);
        }

        final boolean doesBusinessUnitExists = businessUnitRepo.existsByName(businessUnitName);
        if (!doesBusinessUnitExists) {
            return new ResponseEntity<>(new InvalidBusinessUnitResponse(businessUnitName),HttpStatus.BAD_REQUEST);
        }
        final boolean doesCertificationTaskExists = certificationTaskRepo.existsByName(certificationTaskName);
        if (!doesCertificationTaskExists) {
            return new ResponseEntity<>(new InvalidCertificationTaskResponse(certificationTaskName),HttpStatus.BAD_GATEWAY);
        }
        return null;
    }

    private List<ReportResponse> empCertTaskToReportResponse(List<EmpCertTask> allEmpDownOneLevel) {
        List<ReportResponse> reportResponses = new ArrayList<>();


        for (EmpCertTask empCertTask : allEmpDownOneLevel) {
            ReportResponse reportResponse = new ReportResponse();
            reportResponse.setBusinessUnitName(empCertTask.getEmpBusinessUnit().getBusinessUnit().getName());
            reportResponse.setCertifiedOnDate(empCertTask.getCertifiedOn());

            final EmpCertificateTaskDetails submittedDetails = empCertTask.getSubmittedDetails();
            if (submittedDetails != null) {
                reportResponse.setCertificationStatus(submittedDetails.getCertificationStatus());
            } else {
                reportResponse.setCertificationStatus("Pending");
            }

            reportResponse.setDueDate(empCertTask.getDueDate());

            final String employeeId = empCertTask.getEmployeeId();

            reportResponse.setCertifierName(employeeRepo.findByEmpId(employeeId).getName());

            if (submittedDetails != null && submittedDetails.getCertificate() != null ) {
                reportResponse.setCertificateUrl(submittedDetails.getCertificate().getPath());
                reportResponse.setCertificateId(submittedDetails.getCertificate().getId());
            }
            reportResponses.add(reportResponse);
        }
        return reportResponses;
    }


    static class BusinessUnitRequest {
        private String businessUnitName;

        public String getBusinessUnitName() {
            return businessUnitName;
        }

        public void setBusinessUnitName(String businessUnitName) {
            this.businessUnitName = businessUnitName;
        }
    }
//    http://localhost:8080/view_report/list
    @PostMapping("list")
//    ResponseEntity<List<BusinessUnitList>>
    public ResponseEntity<?> getListOfAllBusinessUnitOneLevelBelow(@RequestBody BusinessUnitRequest businessUnitRequest) {
        //for given business unit get the businessUnit below it
        //for given business unit find the units whose parent unit is given one and Business unit matches the given one
        final String businessUnitName = businessUnitRequest.getBusinessUnitName();
        final List<EmpBusinessUnit> businessUnitOneLevelDown = employeeBusinessUnitRepo.findAllByParentBusinessUnitBusinessUnitName(businessUnitName);
        if (businessUnitOneLevelDown == null || businessUnitOneLevelDown.size() == 0) {
            return new ResponseEntity<>(new CertifierCertifyingController.FunctionalityNotAllowedResponse(),HttpStatus.BAD_REQUEST);
        }

        List<String> nameOfBusinessUnits = new ArrayList<>();

        businessUnitOneLevelDown.forEach(empBusinessUnit -> {
            if (empBusinessUnit != null && empBusinessUnit.getBusinessUnit() != null) {
                final String name = empBusinessUnit.getBusinessUnit().getName();
                nameOfBusinessUnits.add(name);
            }
        });

        return new ResponseEntity(nameOfBusinessUnits,HttpStatus.OK);
    }

    //TODO : testing
//    http://localhost:8080/view_report/businessUnit
    @PostMapping("businessUnit")
//    List<ReportResponse>
    public ResponseEntity<?> getAllEmployeeOneLevelBelowWithSpecificBusinessUnit(@RequestBody UnitDetails unitDetails) {
        final String employeeId = unitDetails.getEmployeeId();
        final String businessUnitName = unitDetails.getBusinessUnitName();
        final String certificationTaskName = unitDetails.getNameOfCertificationTask();

        ResponseEntity<?> validationResponse = validateForInputForViewReportingCertifierStatus(employeeId, businessUnitName, certificationTaskName);
        if (validationResponse != null) return validationResponse;

//        Name of the task as well
        final List<EmpCertTask> allReportTasksByBusinessUnit = employeeCertificationTaskRepo
                .findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskName(businessUnitName, employeeId,certificationTaskName);

        final List<ReportResponse> reportResponses = empCertTaskToReportResponse(allReportTasksByBusinessUnit);
        return new ResponseEntity<>(reportResponses,HttpStatus.OK);
    }


    @PostMapping("/filter/dueDate/{asc}")
    public ResponseEntity<?> getAllEmployeeOneLevelBelowWithSpecificBusinessUnitOrderByDueDate(@RequestBody UnitDetails unitDetails, @PathVariable boolean asc) {
        final String employeeId = unitDetails.getEmployeeId();
        final String businessUnitName = unitDetails.getBusinessUnitName();
        final String certificationTaskName = unitDetails.getNameOfCertificationTask();

        ResponseEntity<?> validationResponse = validateForInputForViewReportingCertifierStatus(employeeId, businessUnitName, certificationTaskName);
        if (validationResponse != null) return validationResponse;

        final List<EmpCertTask> allReportTasksByBusinessUnit;
        if (asc) {
            allReportTasksByBusinessUnit = employeeCertificationTaskRepo
                    .findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByDueDateAsc(businessUnitName, employeeId, certificationTaskName);
        } else {
            allReportTasksByBusinessUnit = employeeCertificationTaskRepo
                    .findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByDueDateDesc(businessUnitName, employeeId, certificationTaskName);
        }
        final List<ReportResponse> reportResponses = empCertTaskToReportResponse(allReportTasksByBusinessUnit);
        return new ResponseEntity<>(reportResponses,HttpStatus.OK);
    }

    @PostMapping("/filter/certifiedOnDate/{asc}")
    public ResponseEntity<?> getAllEmployeeOneLevelBelowWithSpecificBusinessUnitOrderByCertifiedOn(@RequestBody UnitDetails unitDetails, @PathVariable boolean asc) {
        final String employeeId = unitDetails.getEmployeeId();
        final String businessUnitName = unitDetails.getBusinessUnitName();
        final String certificationTaskName = unitDetails.getNameOfCertificationTask();

        ResponseEntity<?> validationResponse = validateForInputForViewReportingCertifierStatus(employeeId, businessUnitName, certificationTaskName);
        if (validationResponse != null) return validationResponse;

        final List<EmpCertTask> allReportTasksByBusinessUnit;
        if (asc) {
            allReportTasksByBusinessUnit = employeeCertificationTaskRepo
                    .findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByCertifiedOnAsc(businessUnitName, employeeId, certificationTaskName);
        } else {
            allReportTasksByBusinessUnit = employeeCertificationTaskRepo
                    .findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByCertifiedOnDesc(businessUnitName, employeeId, certificationTaskName);
        }
        final List<ReportResponse> reportResponses = empCertTaskToReportResponse(allReportTasksByBusinessUnit);
        return new ResponseEntity<>(reportResponses,HttpStatus.OK);
    }

    @PostMapping("/filter/certifierName/{asc}")
    public ResponseEntity<?> getAllEmployeeOneLevelBelowWithSpecificBusinessUnitOrderByCertifierName(@RequestBody UnitDetails unitDetails, @PathVariable boolean asc) {
        final String employeeId = unitDetails.getEmployeeId();
        final String businessUnitName = unitDetails.getBusinessUnitName();
        final String certificationTaskName = unitDetails.getNameOfCertificationTask();

        ResponseEntity<?> validationResponse = validateForInputForViewReportingCertifierStatus(employeeId, businessUnitName, certificationTaskName);
        if (validationResponse != null) return validationResponse;

        final List<EmpCertTask> allReportTasksByBusinessUnit;
        if (asc) {
            allReportTasksByBusinessUnit = employeeCertificationTaskRepo
                    .findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByCertifierNameAsc(businessUnitName, employeeId, certificationTaskName);
        } else {
            allReportTasksByBusinessUnit = employeeCertificationTaskRepo
                    .findAllByEmpBusinessUnitBusinessUnitNameAndEmpBusinessUnitReportingToEmpIdAndCertTaskNameOrderByCertifierNameDesc(businessUnitName, employeeId, certificationTaskName);
        }
        final List<ReportResponse> reportResponses = empCertTaskToReportResponse(allReportTasksByBusinessUnit);
        return new ResponseEntity<>(reportResponses,HttpStatus.OK);
    }

}
