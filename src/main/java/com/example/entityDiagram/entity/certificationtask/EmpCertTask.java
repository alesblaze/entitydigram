package com.example.entityDiagram.entity.certificationtask;

import com.example.entityDiagram.entity.businessunit.EmpBusinessUnit;
import com.example.entityDiagram.entity.certificationtaskdetails.EmpCertificateTaskDetails;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class EmpCertTask {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false)
    private Integer id;
    @OneToOne
    private CertTask certTask;
    @OneToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private EmpBusinessUnit empBusinessUnit;
    private String employeeId;
    @JsonSerialize(as = LocalDate.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd-MMM-yyyy")
    private LocalDate dueDate;
    @JsonSerialize(as = LocalDate.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd-MMM-yyyy")
    private LocalDate certifiedOn;
    private String certifierName;
    private boolean isProcessSubmitted = false;
    private boolean isRemarksSubmitted = false;

    @OneToOne
    private EmpCertificateTaskDetails savedDetails;
    @JsonIgnore
    @OneToOne
    private EmpCertificateTaskDetails submittedDetails;

    public EmpCertTask() {
    }

    public EmpCertTask(LocalDate dueDate, String employeeId ,String status){
//        this.status = status;
        this.dueDate = dueDate;
        this.employeeId = employeeId;
    }

    public EmpCertTask(LocalDate dueDate, String employeeId ,String status, EmpCertificateTaskDetails savedDetails, EmpCertificateTaskDetails submittedDetails) {
        this.dueDate = dueDate;
        this.employeeId = employeeId;
        this.savedDetails = savedDetails;
        this.submittedDetails = submittedDetails;
    }

    public boolean isProcessSubmitted() {
        return isProcessSubmitted;
    }

    public void setProcessSubmitted(boolean processSubmitted) {
        isProcessSubmitted = processSubmitted;
    }

    public boolean isRemarksSubmitted() {
        return isRemarksSubmitted;
    }

    public void setRemarksSubmitted(boolean remarksSubmitted) {
        isRemarksSubmitted = remarksSubmitted;
    }

    public CertTask getCertTask() {
        return certTask;
    }

    public void setCertTask(CertTask certTask) {
        this.certTask = certTask;
    }

    public EmpBusinessUnit getEmpBusinessUnit() {
        return empBusinessUnit;
    }

    public void setEmpBusinessUnit(EmpBusinessUnit empBusinessUnit) {
        this.empBusinessUnit = empBusinessUnit;
    }

    public String getCertifierName() {
        return certifierName;
    }

    public void setCertifierName(String certifierName) {
        this.certifierName = certifierName;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public LocalDate getCertifiedOn() {
        return certifiedOn;
    }

    public void setCertifiedOn(LocalDate certifiedOn) {
        this.certifiedOn = certifiedOn;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EmpCertificateTaskDetails getSavedDetails() {
        return savedDetails;
    }

    public void setSavedDetails(EmpCertificateTaskDetails savedDetails) {
        this.savedDetails = savedDetails;
    }

    public EmpCertificateTaskDetails getSubmittedDetails() {
        return submittedDetails;
    }

    public void setSubmittedDetails(EmpCertificateTaskDetails submittedDetails) {
        this.submittedDetails = submittedDetails;
    }
}
