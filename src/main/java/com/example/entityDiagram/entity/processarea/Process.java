package com.example.entityDiagram.entity.processarea;

import javax.persistence.*;

@Entity
public class Process {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer processId;
    @Column(updatable = false)
    private String name;

    public Process() {
    }

    public Process(String name) {
        this.name = name;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
