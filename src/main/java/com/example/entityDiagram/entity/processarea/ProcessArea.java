package com.example.entityDiagram.entity.processarea;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class ProcessArea {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(updatable = false)
    private String name;

    @OneToMany(fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Process> processes;

    public ProcessArea() {
    }

    public ProcessArea(String name) {
        this.name = name;
    }

    public ProcessArea(String name, List<Process> processes) {
        this.name = name;
        this.processes = processes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Process> getProcesses() {
        return processes;
    }

    public void setProcesses(List<Process> processes) {
        this.processes = processes;
    }
}
