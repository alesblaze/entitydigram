package com.example.entityDiagram.entity.certificationtaskdetails;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Certificate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String path;
    private Integer empCertificationTaskDetailsId;

    public Certificate() {
    }

    public Certificate(String path/*, Integer empCertificationTaskDetailsId*/) {
        this.path = path;
//        this.empCertificationTaskDetailsId = empCertificationTaskDetailsId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getEmpCertificationTaskDetailsId() {
        return empCertificationTaskDetailsId;
    }

    public void setEmpCertificationTaskDetailsId(Integer empCertificationTaskDetailsId) {
        this.empCertificationTaskDetailsId = empCertificationTaskDetailsId;
    }
}
