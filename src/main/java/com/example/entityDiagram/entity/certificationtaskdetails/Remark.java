package com.example.entityDiagram.entity.certificationtaskdetails;

import javax.persistence.*;

@Entity
public class Remark {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column
    private Integer empCertificationTaskId;
//    private String empAssetDetailsId;
    private String assetName;
    private String isCompliant;
    @Column(length = 1000,nullable = false)
    private String remarks;


    public Remark() {
    }

    public Remark(String empAssetName,
                  String isCompliant, String remarks) {
        this.assetName = empAssetName;
        this.isCompliant = isCompliant;
        this.remarks = remarks;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEmpCertificationTaskId() {
        return empCertificationTaskId;
    }

    public void setEmpCertificationTaskId(Integer empCertificationTaskId) {
        this.empCertificationTaskId = empCertificationTaskId;
    }

    public String getEmpAssetName() {
        return assetName;
    }

    public void setEmpAssetName(String empAssetName) {
        this.assetName = empAssetName;
    }

    public String getIsCompliant() {
        return isCompliant;
    }

    public void setIsCompliant(String isCompliant) {
        this.isCompliant = isCompliant;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
