package com.example.entityDiagram.entity.certificationtaskdetails;

import com.example.entityDiagram.entity.certificationtaskdetails.AdditionalDocument;
import com.example.entityDiagram.entity.certificationtaskdetails.Remark;
import com.example.entityDiagram.entity.certificationtaskdetails.EmpProcessArea;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class EmpCertificateTaskDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer EmpCertTaskId;
    private String isCompliant;
    private String certificationStatus;
    @OneToMany
    private List<EmpProcessArea> empProcessAreas;
    @OneToMany
    private List<Remark> remarks;
    @OneToMany(cascade = {CascadeType.REMOVE})
    private List<AdditionalDocument> additionalDocuments;
    @OneToOne
    private Certificate certificate;
    private boolean isAdditionalDocumentsSubmitted = false;


    public EmpCertificateTaskDetails() {
    }

    public EmpCertificateTaskDetails(String isCompliant, String certificationStatus, LocalDate certifiedOn) {
        this.isCompliant = isCompliant;
        this.certificationStatus = certificationStatus;
    }

    public boolean isAdditionalDocumentsSubmitted() {
        return isAdditionalDocumentsSubmitted;
    }

    public void setAdditionalDocumentsSubmitted(boolean additionalDocumentsSubmitted) {
        isAdditionalDocumentsSubmitted = additionalDocumentsSubmitted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEmpCertTaskId() {
        return EmpCertTaskId;
    }

    public void setEmpCertTaskId(Integer empCertTaskId) {
        EmpCertTaskId = empCertTaskId;
    }

    public String getIsCompliant() {
        return isCompliant;
    }

    public void setIsCompliant(String isCompliant) {
        this.isCompliant = isCompliant;
    }

    public String getCertificationStatus() {
        return certificationStatus;
    }

    public void setCertificationStatus(String certificationStatus) {
        this.certificationStatus = certificationStatus;
    }

    public List<EmpProcessArea> getEmpProcessAreas() {
        return empProcessAreas;
    }

    public void setEmpProcessAreas(List<EmpProcessArea> empProcessAreas) {
        this.empProcessAreas = empProcessAreas;
    }

    public List<Remark> getRemarks() {
        return remarks;
    }

    public void setRemarks(List<Remark> remarks) {
        this.remarks = remarks;
    }

    public List<AdditionalDocument> getAdditionalDocuments() {
        return additionalDocuments;
    }

    public void setAdditionalDocuments(List<AdditionalDocument> additionalDocuments) {
        this.additionalDocuments = additionalDocuments;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }
}
