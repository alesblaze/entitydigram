package com.example.entityDiagram.entity.certificationtaskdetails;

import com.example.entityDiagram.entity.processarea.Process;

import javax.persistence.*;

@Entity
public class EmpProcess {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @OneToOne
    private Process process;
    private String isCompliant;
    private String severity;
    @Column(length = 1000,nullable = false)
    private String remarks;

    public EmpProcess() {
    }

    public EmpProcess(Process process, String isCompliant, String severity, String remarks) {
        this.process = process;
        this.isCompliant = isCompliant;
        this.severity = severity;
        this.remarks = remarks;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIsCompliant() {
        return isCompliant;
    }

    public void setIsCompliant(String isCompliant) {
        this.isCompliant = isCompliant;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
