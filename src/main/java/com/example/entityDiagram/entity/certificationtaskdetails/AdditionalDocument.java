package com.example.entityDiagram.entity.certificationtaskdetails;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AdditionalDocument {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String fileName;
    private String description;
    private Integer empCertificationTaskDetailsId;
    private String path;

    public AdditionalDocument() {
    }

    public AdditionalDocument(String fileName, String path, String description) {
        this.fileName = fileName;
        this.path = path;
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEmpCertificationTaskDetailsId() {
        return empCertificationTaskDetailsId;
    }

    public void setEmpCertificationTaskDetailsId(Integer empCertificationTaskDetailsId) {
        this.empCertificationTaskDetailsId = empCertificationTaskDetailsId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "AdditionalDocument{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", description='" + description + '\'' +
                ", empCertificationTaskDetailsId=" + empCertificationTaskDetailsId +
                ", path='" + path + '\'' +
                '}';
    }
}
