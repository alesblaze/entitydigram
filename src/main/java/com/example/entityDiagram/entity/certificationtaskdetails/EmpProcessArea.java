package com.example.entityDiagram.entity.certificationtaskdetails;

import com.example.entityDiagram.entity.processarea.ProcessArea;

import javax.persistence.*;
import java.util.List;

@Entity
public class EmpProcessArea {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @OneToOne
    private ProcessArea processArea;
    @Column
    private Integer employeeCertificationTaskId;
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<EmpProcess> empProcesses;

    public EmpProcessArea() {
    }

    public EmpProcessArea(ProcessArea processArea, List<EmpProcess> empProcesses) {
        this.processArea = processArea;
        this.empProcesses = empProcesses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProcessArea getProcessArea() {
        return processArea;
    }

    public void setProcessArea(ProcessArea processArea) {
        this.processArea = processArea;
    }

    public Integer getEmployeeCertificationTaskId() {
        return employeeCertificationTaskId;
    }

    public void setEmployeeCertificationTaskId(Integer employeeCertificationTaskId) {
        this.employeeCertificationTaskId = employeeCertificationTaskId;
    }

    public List<EmpProcess> getEmpProcesses() {
        return empProcesses;
    }

    public void setEmpProcesses(List<EmpProcess> empProcesses) {
        this.empProcesses = empProcesses;
    }
}
