package com.example.entityDiagram.entity.businessunit;

import javax.persistence.*;

@Entity
public class BusinessUnit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    private String name;
    private String category;

    public BusinessUnit(String name, String category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public BusinessUnit(){}

    public BusinessUnit( String name) {
        this.id = id;
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
