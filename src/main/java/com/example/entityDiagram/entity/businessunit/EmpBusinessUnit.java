package com.example.entityDiagram.entity.businessunit;

import com.example.entityDiagram.entity.certificationtask.EmpCertTask;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class EmpBusinessUnit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    public BusinessUnit businessUnit;

    @JsonIgnore
    @OneToOne
    public EmpBusinessUnit parentBusinessUnit;
    private String empId;
    private String role;
    private String reportingToEmpId;

    @JsonIgnore
    @OneToMany(
            orphanRemoval = true,fetch = FetchType.LAZY)
    private List<EmpCertTask> empCertTasks;



    public EmpBusinessUnit(){}


    public EmpBusinessUnit(BusinessUnit businessUnit, String role, List<EmpCertTask> empCertTasks) {
        this.businessUnit = businessUnit;
        this.role = role;
        this.empCertTasks = empCertTasks;
    }

    public EmpBusinessUnit getParentBusinessUnit() {
        return parentBusinessUnit;
    }

    public void setParentBusinessUnit(EmpBusinessUnit parentBusinessUnit) {
        this.parentBusinessUnit = parentBusinessUnit;
    }

    public String getReportingToEmpId() {
        return reportingToEmpId;
    }

    public void setReportingToEmpId(String reportingToEmpId) {
        this.reportingToEmpId = reportingToEmpId;
    }

    public BusinessUnit getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(BusinessUnit businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<EmpCertTask> getEmpCertTasks() {
        return empCertTasks;
    }

    public void setEmpCertTasks(List<EmpCertTask> empCertTasks) {
        this.empCertTasks = empCertTasks;
    }
}
