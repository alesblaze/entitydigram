package com.example.entityDiagram.entity;

import com.example.entityDiagram.entity.businessunit.EmpBusinessUnit;
import com.example.entityDiagram.entity.employeedetails.Company;
import com.example.entityDiagram.entity.employeedetails.Function;
import com.example.entityDiagram.entity.employeedetails.Geography;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column
    private String empId;
    @Column
    private String name;
    @Column
    private String password;
    @OneToOne
    private Function function;
    @OneToOne
    private Geography geography;
    @OneToOne
    private Company company;

    @OneToMany
    @JsonIgnore
    private List<EmpBusinessUnit> empBusinessUnitList;

    public Employee(){}

    public Employee(String empId, String name, String password) {
        this.empId = empId;
        this.name = name;
        this.password = password;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public Geography getGeography() {
        return geography;
    }

    public void setGeography(Geography geography) {
        this.geography = geography;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<EmpBusinessUnit> getEmpBusinessUnitList() {
        return empBusinessUnitList;
    }

    public void setEmpBusinessUnitList(List<EmpBusinessUnit> empBusinessUnitList) {
        this.empBusinessUnitList = empBusinessUnitList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<EmpBusinessUnit> getBusinessUnitList() {
        return empBusinessUnitList;
    }

    public void setBusinessUnitList(List<EmpBusinessUnit> empBusinessUnitList) {
        this.empBusinessUnitList = empBusinessUnitList;
    }
}
