package com.example.entityDiagram.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.LocalDate;

public class CertificationTaskItem {
    private Integer certificateTaskId;
    private String taskName;
    @JsonSerialize(as = LocalDate.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd-MMM-yyyy")
    private LocalDate startDate;
    @JsonSerialize(as = LocalDate.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd-MMM-yyyy")
    private LocalDate endDate;

    public CertificationTaskItem() {
    }

    public CertificationTaskItem(Integer certificateTaskId, String taskName, LocalDate startDate, LocalDate endDate) {
        this.certificateTaskId = certificateTaskId;
        this.taskName = taskName;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getCertificateTaskId() {
        return certificateTaskId;
    }

    public void setCertificateTaskId(Integer certificateTaskId) {
        this.certificateTaskId = certificateTaskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}