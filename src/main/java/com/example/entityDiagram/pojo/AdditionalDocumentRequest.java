package com.example.entityDiagram.pojo;

public class AdditionalDocumentRequest {
    private Integer additionalDetailsId;
    private Integer empCertificationTaskDetailsId;
    private String employeeId;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getAdditionalDetailsId() {
        return additionalDetailsId;
    }

    public void setAdditionalDetailsId(Integer additionalDetailsId) {
        this.additionalDetailsId = additionalDetailsId;
    }

    public Integer getEmpCertificationTaskDetailsId() {
        return empCertificationTaskDetailsId;
    }

    public void setEmpCertificationTaskDetailsId(Integer empCertificationTaskDetailsId) {
        this.empCertificationTaskDetailsId = empCertificationTaskDetailsId;
    }
}