package com.example.entityDiagram.pojo;

public class CertifierReport {
    String empId;
    String businessUnitName;
    String certificationTaskName;

    public String getCertificationTaskName() {
        return certificationTaskName;
    }

    public void setCertificationTaskName(String certificationTaskName) {
        this.certificationTaskName = certificationTaskName;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }
}
