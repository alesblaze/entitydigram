package com.example.entityDiagram.pojo;

public class BusinessUnitList {
    private String businessUnitName;

    public BusinessUnitList() {
    }

    public BusinessUnitList(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }
}
