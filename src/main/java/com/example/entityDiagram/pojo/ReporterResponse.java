package com.example.entityDiagram.pojo;

public class ReporterResponse {
    String nameOfReporter;
    String reporterRole;
    String parentBusinessUnitName;

    public ReporterResponse() {
    }

    public ReporterResponse(String nameOfReporter, String reporterRole, String parentBusinessUnitName) {
        this.nameOfReporter = nameOfReporter;
        this.reporterRole = reporterRole;
        this.parentBusinessUnitName = parentBusinessUnitName;
    }

    public String getNameOfReporter() {
        return nameOfReporter;
    }

    public void setNameOfReporter(String nameOfReporter) {
        this.nameOfReporter = nameOfReporter;
    }

    public String getReporterRole() {
        return reporterRole;
    }

    public void setReporterRole(String reporterRole) {
        this.reporterRole = reporterRole;
    }

    public String getParentBusinessUnitName() {
        return parentBusinessUnitName;
    }

    public void setParentBusinessUnitName(String parentBusinessUnitName) {
        this.parentBusinessUnitName = parentBusinessUnitName;
    }
}