package com.example.entityDiagram.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.LocalDate;

public class ReportResponse {
    private String businessUnitName;
    private String certifierName;
    @JsonSerialize(as = LocalDate.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MMM-yyyy")
    private LocalDate dueDate;
    @JsonSerialize(as = LocalDate.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MMM-yyyy")
    private LocalDate certifiedOnDate;
    private boolean IsCompliant;
    private String certificationStatus;
    private String certificateUrl;
    private Integer certificateId;

    public ReportResponse() {
    }

    public ReportResponse(String businessUnitName, String certifierName, LocalDate dueDate, LocalDate certifiedOnDate, boolean isCompliant, String complianceStatus, String certificateUrl) {
        this.businessUnitName = businessUnitName;
        this.certifierName = certifierName;
        this.dueDate = dueDate;
        this.certifiedOnDate = certifiedOnDate;
        IsCompliant = isCompliant;
        certificationStatus = complianceStatus;
        this.certificateUrl = certificateUrl;
    }

    public Integer getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Integer certificateId) {
        this.certificateId = certificateId;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getCertifierName() {
        return certifierName;
    }

    public void setCertifierName(String certifierName) {
        this.certifierName = certifierName;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getCertifiedOnDate() {
        return certifiedOnDate;
    }

    public void setCertifiedOnDate(LocalDate certifiedOnDate) {
        this.certifiedOnDate = certifiedOnDate;
    }

    public boolean isCompliant() {
        return IsCompliant;
    }

    public void setCompliant(boolean compliant) {
        IsCompliant = compliant;
    }

    public String getCertificationStatus() {
        return certificationStatus;
    }

    public void setCertificationStatus(String certificationStatus) {
        this.certificationStatus = certificationStatus;
    }

    public String getCertificateUrl() {
        return certificateUrl;
    }

    public void setCertificateUrl(String certificateUrl) {
        this.certificateUrl = certificateUrl;
    }
}
