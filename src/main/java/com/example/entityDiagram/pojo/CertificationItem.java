package com.example.entityDiagram.pojo;

public class CertificationItem {
    private Integer certificationTaskId;
    private String certificationTaskName;

    public CertificationItem() {
    }

    public CertificationItem(Integer certificationTaskId, String certificationTaskName) {
        this.certificationTaskId = certificationTaskId;
        this.certificationTaskName = certificationTaskName;
    }

    public Integer getCertificationTaskId() {
        return certificationTaskId;
    }

    public void setCertificationTaskId(Integer certificationTaskId) {
        this.certificationTaskId = certificationTaskId;
    }

    public String getCertificationTaskName() {
        return certificationTaskName;
    }

    public void setCertificationTaskName(String certificationTaskName) {
        this.certificationTaskName = certificationTaskName;
    }
}
