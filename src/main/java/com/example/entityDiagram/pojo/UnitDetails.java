package com.example.entityDiagram.pojo;

public class UnitDetails {
    private String employeeId;
    private String nameOfCertificationTask;
    private String businessUnitName;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getNameOfCertificationTask() {
        return nameOfCertificationTask;
    }

    public void setNameOfCertificationTask(String nameOfCertificationTask) {
        this.nameOfCertificationTask = nameOfCertificationTask;
    }
    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }
}
