package com.example.entityDiagram.pojo;


import com.example.entityDiagram.entity.certificationtask.EmpCertTask;

public class CertificateTaskResponse {
    CertifierResponse certifier;
    ReporterResponse reporter;
    EmpCertTask empCertTask;

    public CertificateTaskResponse() {
    }

    public CertificateTaskResponse(CertifierResponse certifier, ReporterResponse reporter, EmpCertTask empCertTask) {
        this.certifier = certifier;
        this.reporter = reporter;
        this.empCertTask = empCertTask;
    }

    public CertifierResponse getCertifier() {
        return certifier;
    }

    public void setCertifier(CertifierResponse certifier) {
        this.certifier = certifier;
    }

    public ReporterResponse getReporter() {
        return reporter;
    }

    public void setReporter(ReporterResponse reporter) {
        this.reporter = reporter;
    }

    public EmpCertTask getEmpCertTask() {
        return empCertTask;
    }

    public void setEmpCertTask(EmpCertTask empCertTask) {
        this.empCertTask = empCertTask;
    }
}