package com.example.entityDiagram.pojo;

import com.example.entityDiagram.entity.certificationtaskdetails.Remark;

import java.util.List;

public class RemarksRequest {
    private String employeeId;
    private Integer empCertificationTaskId;
    private List<Remark> remarks;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getEmpCertificationTaskId() {
        return empCertificationTaskId;
    }

    public void setEmpCertificationTaskId(Integer empCertificationTaskId) {
        this.empCertificationTaskId = empCertificationTaskId;
    }

    public List<Remark> getRemarks() {
        return remarks;
    }

    public void setRemarks(List<Remark> remarks) {
        this.remarks = remarks;
    }
}