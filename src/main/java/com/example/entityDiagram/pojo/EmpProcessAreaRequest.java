package com.example.entityDiagram.pojo;

import com.example.entityDiagram.entity.certificationtaskdetails.EmpProcessArea;

import java.util.List;

public class EmpProcessAreaRequest {
    String employeeId;
    Integer empCertificationTaskId;
    List<EmpProcessArea> empProcessAreas;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getEmpCertificationTaskId() {
        return empCertificationTaskId;
    }

    public void setEmpCertificationTaskId(Integer empCertificationTaskId) {
        this.empCertificationTaskId = empCertificationTaskId;
    }

    public List<EmpProcessArea> getEmpProcessAreas() {
        return empProcessAreas;
    }

    public void setEmpProcessArea(List<EmpProcessArea> empProcessAreas) {
        this.empProcessAreas = empProcessAreas;
    }
}