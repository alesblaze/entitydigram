package com.example.entityDiagram.pojo;

public class CertifierResponse {
    private String employeeName;
    private String role;
    private String businessUnitName;
    private String employeeFunction;
    private String employeeGeography;
    private String employeeCompany;

    public CertifierResponse() {
    }

    public CertifierResponse(String employeeName, String role, String businessName, String employeeCompany, String employeeGeography, String employeeFunction ) {
        this.employeeCompany = employeeCompany;
        this.employeeGeography = employeeGeography;
        this.employeeFunction = employeeFunction;
        this.employeeName = employeeName;
        this.role = role;
        this.businessUnitName = businessName;
    }

    public String getEmployeeFunction() {
        return employeeFunction;
    }

    public void setEmployeeFunction(String employeeFunction) {
        this.employeeFunction = employeeFunction;
    }

    public String getEmployeeGeography() {
        return employeeGeography;
    }

    public void setEmployeeGeography(String employeeGeography) {
        this.employeeGeography = employeeGeography;
    }

    public String getEmployeeCompany() {
        return employeeCompany;
    }

    public void setEmployeeCompany(String employeeCompany) {
        this.employeeCompany = employeeCompany;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }
}